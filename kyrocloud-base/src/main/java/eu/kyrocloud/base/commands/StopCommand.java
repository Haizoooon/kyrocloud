package eu.kyrocloud.base.commands;

/*

  » eu.kyrocloud.launcher.commands

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 20.03.2021 / 20:57

 */

import eu.kyrocloud.api.commands.Command;
import eu.kyrocloud.api.commands.CommandType;
import eu.kyrocloud.api.commands.ICommandSender;
import eu.kyrocloud.api.commands.IConsoleCommand;
import eu.kyrocloud.api.console.Colors;
import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.base.CloudBase;
import eu.kyrocloud.launcher.CloudBootstrap;
import org.jline.reader.Candidate;

import java.util.List;

@Command(name = "stop", type = CommandType.CONSOLE)
public class StopCommand extends IConsoleCommand {

    @Override
    public void handle(ICommandSender sender, String[] args) {
        if (args[0].equalsIgnoreCase(getCommand())) {
            if(args.length == 1){
                CloudBootstrap.getInstance().getModuleManager().getModules().forEach(module -> {
                    CloudBootstrap.getInstance().getLogger().write(LogType.INFO, "Unloading module " + Colors.GREEN + module.getName() + " " + module.getVersion());
                });

                CloudBase.getInstance().getCloudServiceManager().getCloudServices().forEach(cloudService -> {
                    CloudBootstrap.getInstance().getLogger().write(LogType.INFO, cloudService.getServiceIdName() + " is stopping");
                    cloudService.stop(false);
                });

                CloudBase.getInstance().getNettyServerConnector().stopListening();
                CloudBootstrap.getInstance().getLogger().stopWriter();
                CloudBootstrap.getInstance().getConsoleManager().stopThread();
                System.exit(1);

            }
        }
    }

    @Override
    public List<Candidate> getSuggestions() {
        return null;
    }

    @Override
    public String[] getUsage() {
        return new String[0];
    }
}
