package eu.kyrocloud.base.commands;

/*

  » eu.kyrocloud.base.commands

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 25.03.2021 / 22:10

 */

import eu.kyrocloud.api.commands.Command;
import eu.kyrocloud.api.commands.CommandType;
import eu.kyrocloud.api.commands.ICommandSender;
import eu.kyrocloud.api.commands.IConsoleCommand;
import eu.kyrocloud.base.CloudBase;
import eu.kyrocloud.launcher.CloudBootstrap;
import org.jline.reader.Candidate;

import java.util.List;

@Command(name = "reload", type = CommandType.CONSOLE)
public class ReloadCommand extends IConsoleCommand {
    @Override
    public List<Candidate> getSuggestions() {
        return null;
    }

    @Override
    public void handle(ICommandSender sender, String[] args) {
        if (args[0].equalsIgnoreCase(getCommand())) {
            CloudBootstrap.getInstance().getModuleManager().unregisterModules();
            CloudBootstrap.getInstance().getModuleManager().registerModules();
        }
    }

    @Override
    public String[] getUsage() {
        return new String[0];
    }
}
