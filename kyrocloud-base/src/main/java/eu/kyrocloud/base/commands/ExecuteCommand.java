package eu.kyrocloud.base.commands;

import com.google.common.collect.Lists;
import eu.kyrocloud.api.commands.Command;
import eu.kyrocloud.api.commands.CommandType;
import eu.kyrocloud.api.commands.ICommandSender;
import eu.kyrocloud.api.commands.IConsoleCommand;
import eu.kyrocloud.api.console.Colors;
import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.api.service.ICloudService;
import eu.kyrocloud.base.CloudBase;
import eu.kyrocloud.base.service.CloudService;
import eu.kyrocloud.launcher.CloudBootstrap;
import org.jline.reader.Candidate;

import java.util.List;

/*

  » eu.kyrocloud.base.commands

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 23.03.2021 / 11:59

 */
@Command(name = "execute", type = CommandType.CONSOLE)
public class ExecuteCommand extends IConsoleCommand {

    @Override
    public void handle(ICommandSender sender, String[] args) {
        if (args[0].equalsIgnoreCase(getCommand())) {
            if(args.length > 2){
                CloudService cloudService = CloudBase.getInstance().getCloudServiceManager().getCloudServices().stream().filter(cloudServices -> cloudServices.getServiceIdName().equalsIgnoreCase(args[1])).findAny().orElse(null);
                if(cloudService == null){
                    return;
                }

                StringBuilder stringBuilder = new StringBuilder();
                for(int i = 2; i < args.length; i++){
                    stringBuilder.append(args[i]).append(" ");
                }

                cloudService.getProcessManager().executeCommand(stringBuilder.toString());
                CloudBootstrap.getInstance().getLogger().write(LogType.INFO, "Trying to send " + Colors.RED + stringBuilder.toString() + Colors.RESET + " to service " + Colors.GREEN + cloudService.getServiceIdName());

            }
        }
    }

    @Override
    public List<Candidate> getSuggestions() {
        List<Candidate> candidates = Lists.newArrayList();
        for(CloudService cloudService : CloudBase.getInstance().getCloudServiceManager().getCloudServices()){
            candidates.add(new Candidate(cloudService.getServiceIdName()));
        }
        return candidates;
    }


    @Override
    public String[] getUsage() {
        return new String[0];
    }
}