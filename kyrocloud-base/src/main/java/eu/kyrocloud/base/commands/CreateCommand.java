package eu.kyrocloud.base.commands;

/*

  » eu.kyrocloud.launcher.commands

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 20.03.2021 / 11:08

 */

import eu.kyrocloud.api.commands.Command;
import eu.kyrocloud.api.commands.CommandType;
import eu.kyrocloud.api.commands.ICommandSender;
import eu.kyrocloud.api.commands.IConsoleCommand;
import eu.kyrocloud.base.setups.GroupSetup;
import eu.kyrocloud.base.setups.TemplateSetup;
import eu.kyrocloud.launcher.CloudBootstrap;
import eu.kyrocloud.launcher.setup.setups.ExampleSetup;
import eu.kyrocloud.launcher.setup.setups.IpSetup;
import org.jline.reader.Candidate;

import java.util.List;

@Command(name = "create", type = CommandType.CONSOLE)
public class CreateCommand extends IConsoleCommand {

    @Override
    public void handle(ICommandSender sender, String[] args) {
        if (args[0].equalsIgnoreCase(getCommand())) {
            if(args.length >= 2){
                switch (args[1]){

                    case "template":
                        CloudBootstrap.getInstance().getLogger().clear();
                        new TemplateSetup();
                        break;

                    case "group":
                        CloudBootstrap.getInstance().getLogger().clear();
                        new GroupSetup();
                        break;

                }
            }
        }
    }

    @Override
    public List<Candidate> getSuggestions() {
        return null;
    }

    @Override
    public String[] getUsage() {
        return new String[]{"create group × open the setup of a group", "create template × open the setup of a template"};
    }
}
