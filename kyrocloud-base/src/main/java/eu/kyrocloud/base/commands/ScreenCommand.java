package eu.kyrocloud.base.commands;

/*

  » eu.kyrocloud.launcher.commands

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 22.03.2021 / 18:40

 */

import com.google.common.collect.Lists;
import eu.kyrocloud.api.commands.Command;
import eu.kyrocloud.api.commands.CommandType;
import eu.kyrocloud.api.commands.ICommandSender;
import eu.kyrocloud.api.commands.IConsoleCommand;
import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.base.CloudBase;
import eu.kyrocloud.base.service.CloudService;
import eu.kyrocloud.launcher.CloudBootstrap;
import org.jline.reader.Candidate;

import java.util.List;

@Command(name = "screen", type = CommandType.CONSOLE)
public class ScreenCommand extends IConsoleCommand {

    @Override
    public void handle(ICommandSender sender, String[] args) {
        if (args[0].equalsIgnoreCase(getCommand())) {
            if(args.length == 2) {
                CloudService cloudService = CloudBase.getInstance().getCloudServiceManager().getCloudServices().stream().filter(cloudServices -> cloudServices.getServiceIdName().equalsIgnoreCase(args[1])).findAny().orElse(null);

                if(cloudService == null){
                    return;
                }

                cloudService.getCachedScreenMessages().forEach(message -> {
                    CloudBootstrap.getInstance().getLogger().write(LogType.INFO, message);
                });

            }
        }
    }

    @Override
    public List<Candidate> getSuggestions() {
        List<Candidate> candidates = Lists.newArrayList();
        for(CloudService cloudService : CloudBase.getInstance().getCloudServiceManager().getCloudServices()){
            candidates.add(new Candidate(cloudService.getServiceIdName()));
        }
        return candidates;
    }


    @Override
    public String[] getUsage() {
        return new String[0];
    }
}
