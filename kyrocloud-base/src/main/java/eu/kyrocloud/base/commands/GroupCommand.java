package eu.kyrocloud.base.commands;

import eu.kyrocloud.api.commands.Command;
import eu.kyrocloud.api.commands.CommandType;
import eu.kyrocloud.api.commands.ICommandSender;
import eu.kyrocloud.api.commands.IConsoleCommand;
import eu.kyrocloud.api.console.Colors;
import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.base.CloudBase;
import eu.kyrocloud.launcher.CloudBootstrap;
import org.jline.reader.Candidate;

import java.util.List;

@Command(name = "group", type = CommandType.CONSOLE)
public class GroupCommand extends IConsoleCommand {

    @Override
    public void handle(ICommandSender sender, String[] args) {
        if(args[0].equalsIgnoreCase(getCommand())) {
            if(args.length == 2) {
                if (args[1].equalsIgnoreCase("list")) {
                    if(CloudBase.getInstance().getCloudGroupServiceManager().getGroupServices().isEmpty()){
                        CloudBootstrap.getInstance().getLogger().write(LogType.ERROR, "Could not find any group!");
                        return;
                    }
                    CloudBase.getInstance().getCloudGroupServiceManager().getGroupServices().forEach(cloudGroupService -> CloudBootstrap.getInstance().getLogger().write(LogType.INFO, Colors.GREEN + cloudGroupService.getName() + Colors.RESET + " - " + Colors.GREEN + cloudGroupService.getGroupType().getDisplay()));
                }
            }
        }
    }

    @Override
    public List<Candidate> getSuggestions() {
        return null;
    }

    @Override
    public String[] getUsage() {
        return new String[]{"group list × lists all the groups", "group ??? × ???"};
    }
}
