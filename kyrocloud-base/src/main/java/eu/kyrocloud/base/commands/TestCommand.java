package eu.kyrocloud.base.commands;

/*

  » eu.kyrocloud.base.commands

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 26.03.2021 / 13:46

 */

import eu.kyrocloud.api.commands.Command;
import eu.kyrocloud.api.commands.CommandType;
import eu.kyrocloud.api.commands.ICommandSender;
import eu.kyrocloud.api.commands.IConsoleCommand;
import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.api.player.ICloudPlayer;
import eu.kyrocloud.launcher.console.ConsoleSender;
import org.jline.reader.Candidate;

import java.util.List;

@Command(name = "help", type = CommandType.CONSOLE_AND_INGAME)
public class TestCommand extends IConsoleCommand {
    @Override
    public List<Candidate> getSuggestions() {
        return null;
    }

    @Override
    public void handle(ICommandSender sender, String[] args) {
        if(sender instanceof ConsoleSender) {

            sender.sendMessage(LogType.INFO, "OMG DU SPASTEN");

        } else if (sender instanceof ICloudPlayer) {

            sender.sendMessage("DU HUUURE");

        }
    }

    @Override
    public String[] getUsage() {
        return new String[0];
    }
}
