package eu.kyrocloud.base.netty.packets.abstracts;

/*

  » eu.kyrocloud.api.packet

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 21.03.2021 / 18:34

 */

import eu.kyrocloud.base.service.CloudService;

public abstract class ServerPacket {
    private final String packetName;

    public ServerPacket(String packetName) {
        this.packetName = packetName;
    }

    public abstract void read(CloudService cloudService, String value);
    public abstract void write(CloudService cloudService, String value);

    public String getPacketName() {
        return packetName;
    }
}
