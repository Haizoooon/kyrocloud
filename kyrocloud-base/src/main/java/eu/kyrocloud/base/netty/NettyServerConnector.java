package eu.kyrocloud.base.netty;

import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.launcher.CloudBootstrap;

public class NettyServerConnector {

    private final int port;
    private Thread thread;
    private NettyListener nettyListener;

    public NettyServerConnector(int port) {
        this.port = port;
    }

    public NettyServerConnector listening() {
        CloudBootstrap.getInstance().getLogger().write(LogType.INFO, "Trying to bind to port " + port + " (TCP)");
        thread = new Thread(nettyListener = new NettyListener(port));
        thread.start();
        return this;
    }


    public void stopListening() {
        this.nettyListener.group.shutdownGracefully();
        this.nettyListener.workerGroup.shutdownGracefully();
        this.thread.stop();
    }


}
