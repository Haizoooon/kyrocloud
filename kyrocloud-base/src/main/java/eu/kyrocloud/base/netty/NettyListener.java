package eu.kyrocloud.base.netty;

import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.launcher.CloudBootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class NettyListener implements Runnable {

    private final int port;
    EventLoopGroup group;
    EventLoopGroup workerGroup;

    public NettyListener(int port) {
        this.port = port;
    }

    @Override
    public void run() {
        group = new NioEventLoopGroup();
        workerGroup = new NioEventLoopGroup();
        try{
            ServerBootstrap bootstrap = new ServerBootstrap().group(group, workerGroup)
                    .channel(NioServerSocketChannel.class).childHandler(new ServerInitializer());
            try {
                bootstrap.bind("127.0.0.1", port).sync().channel().closeFuture().sync();
            } catch (InterruptedException e) {
                CloudBootstrap.getInstance().getLogger().write(LogType.WARNING, "The cloud is not listening on port " + port + "!");
            }
        } finally{
            group.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }


    public EventLoopGroup getGroup() {
        return group;
    }

    public EventLoopGroup getWorkerGroup() {
        return workerGroup;
    }
}

