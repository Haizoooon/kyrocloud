package eu.kyrocloud.base.netty.packets.packet;

/*

  » de.stickmc.cloud.netty.packets.packet

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 08.03.2021 / 21:53

 */

import eu.kyrocloud.api.group.GroupType;
import eu.kyrocloud.api.netty.NettyProcess;
import eu.kyrocloud.api.service.ICloudService;
import eu.kyrocloud.base.CloudBase;
import eu.kyrocloud.base.netty.packets.abstracts.ServerPacket;
import eu.kyrocloud.base.service.CloudService;
import eu.kyrocloud.jsonlib.JsonLib;
import org.json.JSONObject;

import java.util.Arrays;

public class ServerAddPacket extends ServerPacket {

    public ServerAddPacket() {
        super("serverAddPacket");
    }

    @Override
    public void read(CloudService cloudService, String value) {

    }

    @Override
    public void write(CloudService service, String value) {
        JsonLib jsonLib = new JsonLib();
        jsonLib.append("serviceName", service.getName());
        jsonLib.append("serviceState", service.getServiceState().getDisplay());
        jsonLib.append("groupType", service.getGroupType().getDisplay());
        jsonLib.append("groupVersion", service.getGroupVersion().getDisplay());
        jsonLib.append("serviceId", service.getServiceId());
        jsonLib.append("port", service.getPort());

        CloudService proxyService = CloudBase.getInstance().getCloudServiceManager().getCloudServices().stream().filter(serviceStream -> serviceStream.getGroupType().equals(GroupType.PROXY)).findAny().orElse(null);

        if(proxyService == null){
            return;
        }

        proxyService.getChannel().writeAndFlush(NettyProcess.SERVER_PROXY_ADD + ";" + jsonLib.appendAll() + " \r\n");
    }

}
