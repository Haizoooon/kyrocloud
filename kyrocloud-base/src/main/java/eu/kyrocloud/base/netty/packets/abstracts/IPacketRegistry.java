package eu.kyrocloud.base.netty.packets.abstracts;

/*

  » eu.kyrocloud.api.packet

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 21.03.2021 / 13:58

 */

public interface IPacketRegistry {

    void registerPacket(ServerPacket packet);

}
