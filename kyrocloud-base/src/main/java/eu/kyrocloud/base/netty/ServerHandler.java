package eu.kyrocloud.base.netty;

import eu.kyrocloud.api.console.Colors;
import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.api.netty.NettyProcess;
import eu.kyrocloud.base.CloudBase;
import eu.kyrocloud.base.service.CloudService;
import eu.kyrocloud.launcher.CloudBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.json.JSONObject;

import java.util.Arrays;

public class ServerHandler extends SimpleChannelInboundHandler<String> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) {

        if(msg.length() == 0){
            ctx.channel().close();
            return;
        }

        String[] split = msg.replaceAll("#", "").split(";");
        String type = split[0];
        NettyProcess process = Arrays.stream(NettyProcess.values()).filter(values -> values.toString().equals(type)).findAny().orElse(null);



        if(process == NettyProcess.SERVER_PROXY_PLAYER_CONNECTED || process == NettyProcess.SERVER_PROXY_PLAYER_DISCONNECTED){
            CloudBootstrap.getInstance().getLogger().write(LogType.INFO, split[2]);
        }

        if(process == NettyProcess.SERVER_PROXY_PLAYER_REGISTERED){

            String content = split[2];

            JSONObject jsonObject = new JSONObject(content);
            String playerName = jsonObject.getString("playerName");
            String uuid = jsonObject.getString("uuid");
            String address = jsonObject.getString("address");

            //TODO: register player

        }

        if(process == null){
            CloudBootstrap.getInstance().getLogger().write(LogType.INFO, "Unknown command from service...");
            return;
        }

        CloudBase.getInstance().getCloudServiceManager().processBuild(process, split, ctx.channel());
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) {
        Channel incoming = ctx.channel();
        CloudService cloudService = CloudBase.getInstance().getCloudServiceManager().getCloudServices().stream().filter(cloudServices -> cloudServices.getPort() == Integer.parseInt(incoming.remoteAddress().toString().split(":")[1])).findAny().orElse(null);

        CloudBootstrap.getInstance().getLogger().write(LogType.INFO, "The service " + Colors.GREEN + incoming.remoteAddress() + Colors.RESET + " is now disconnected.");
    }
}
