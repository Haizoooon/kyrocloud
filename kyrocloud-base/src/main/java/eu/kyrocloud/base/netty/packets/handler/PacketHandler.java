package eu.kyrocloud.base.netty.packets.handler;

/*

  » de.stickmc.cloud.netty.packets.handler

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 07.03.2021 / 18:06

 */

import com.google.common.collect.Lists;
import eu.kyrocloud.base.netty.packets.abstracts.IPacketRegistry;
import eu.kyrocloud.base.netty.packets.abstracts.ServerPacket;

import java.util.List;

public class PacketHandler implements IPacketRegistry {

    private final List<ServerPacket> packets = Lists.newArrayList();

    @Override
    public void registerPacket(ServerPacket packet) {
        packets.add(packet);
    }

    public ServerPacket getPacketByName(String name){
        return packets.stream().filter(packet -> packet.getPacketName().equalsIgnoreCase(name)).findAny().orElse(null);
    }

    public List<ServerPacket> getPackets() {
        return packets;
    }

}
