package eu.kyrocloud.base.netty.packets.packet;

/*

  » de.stickmc.cloud.netty.packets.packet

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 07.03.2021 / 18:08

 */

import eu.kyrocloud.api.group.GroupType;
import eu.kyrocloud.api.netty.NettyProcess;
import eu.kyrocloud.api.service.ICloudService;
import eu.kyrocloud.base.CloudBase;
import eu.kyrocloud.base.netty.packets.abstracts.ServerPacket;
import eu.kyrocloud.base.service.CloudService;
import org.json.JSONObject;

public class ServerStopPacket extends ServerPacket {

    public ServerStopPacket() {
        super("serverStopPacket");
    }

    @Override
    public void read(CloudService cloudService, String value) {

    }

    @Override
    public void write(CloudService service, String value) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("serviceName", service.getServiceIdName());
        jsonObject.put("groupType", service.getGroupType());

        CloudService proxyService = CloudBase.getInstance().getCloudServiceManager().getCloudServices().stream().filter(serviceStream -> serviceStream.getGroupType().equals(GroupType.PROXY)).findAny().orElse(null);

        if(proxyService == null){
            return;
        }

        proxyService.getChannel().writeAndFlush(NettyProcess.SERVER_PROXY_REMOVE + ";" + jsonObject.toString() + "\r\n");
    }

    @Override
    public String getPacketName() {
        return super.getPacketName();
    }
}
