package eu.kyrocloud.base.template;

/*

  » eu.kyrocloud.base.template

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 21.03.2021 / 15:43

 */

import com.google.common.collect.Lists;
import eu.kyrocloud.api.console.Colors;
import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.api.template.ITemplate;
import eu.kyrocloud.api.template.ITemplateManager;
import eu.kyrocloud.base.template.sql.TemplateSqlAdapter;
import eu.kyrocloud.launcher.CloudBootstrap;

import java.io.File;
import java.util.List;
import java.util.UUID;

public class TemplateManager extends TemplateSqlAdapter implements ITemplateManager {

    private final List<ITemplate> templates = Lists.newArrayList();

    public TemplateManager() {
        createTemplateFolder();
        this.templates.addAll(getAllTemplates());
    }

    public void createTemplateFolder() {
        if (new File("templates").mkdirs()) {
            CloudBootstrap.getInstance().getLogger().write(LogType.INFO, "creating folder for the template...");
        }
    }

    public void createTemplateFolder(String template) {
        if (new File("templates/" + template).mkdirs()) {
            CloudBootstrap.getInstance().getLogger().write(LogType.INFO, "creating folder for the template whit name: " + template);
        }
    }

    @Override
    public void deleteTemplate(String name) {
        if (templates.stream().anyMatch(temp -> temp.getName().equalsIgnoreCase(name))) {
            templates.remove(new Template(name));
        }
    }

    @Override
    public void createTemplate(String name) {
        if (templates.stream().anyMatch(temp -> temp.getName().equalsIgnoreCase(name))) {
            CloudBootstrap.getInstance().getLogger().write(LogType.ERROR, "Cloud template ist already exiting!");
            return;
        }
        createTemplateFolder(name);
        this.templates.add(new Template(name));
        addTemplate(name);
        CloudBootstrap.getInstance().getLogger().write(LogType.INFO, "Successfully created the template " + Colors.GREEN + name);
    }
}
