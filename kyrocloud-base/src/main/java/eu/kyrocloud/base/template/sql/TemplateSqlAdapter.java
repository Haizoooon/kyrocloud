package eu.kyrocloud.base.template.sql;

/*

  » eu.kyrocloud.base.template.sql

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 21.03.2021 / 15:43

 */

import com.google.common.collect.Lists;
import eu.kyrocloud.api.database.SqlDataType;
import eu.kyrocloud.api.template.ITemplate;
import eu.kyrocloud.base.CloudBase;
import eu.kyrocloud.launcher.CloudBootstrap;
import eu.kyrocloud.launcher.database.SqlAdapter;
import eu.kyrocloud.base.template.Template;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public abstract class TemplateSqlAdapter {


    private final String templateTable = "cloud_templates";
    private final String[] templatesKeys = new String[]{"templateName"};
    private final SqlDataType[] templatesTypes = new SqlDataType[]{SqlDataType.VARCHAR};
    private final SqlAdapter sqlAdapter;

    public TemplateSqlAdapter() {
        this.sqlAdapter = CloudBootstrap.getInstance().getDatabaseHandler().getBaseSqlAdapter();
        createTable();
    }

    public void createTable(){
        this.sqlAdapter.createTable(templateTable, sqlAdapter.getTableInformation(templatesKeys, templatesTypes));
    }

    public boolean isTemplateExist(String template){
        return this.sqlAdapter.existsInTable(templateTable, templatesKeys[0], template);
    }

    public void addTemplate(String template){
        this.sqlAdapter.addMoreInTable(templateTable, Arrays.asList(templatesKeys), Collections.singletonList(template));
    }

    public List<ITemplate> getAllTemplates() {
        List<ITemplate> list = sqlAdapter.getSqlBaseExecutor().executeQuery("SELECT " + templatesKeys[0] + " FROM " + templateTable, resultSet -> {
            List<ITemplate> content = Lists.newArrayList();
            while (resultSet.next()) content.add(new Template(resultSet.getString(templatesKeys[0])));
            return content;
        }, Lists.newArrayList());
        return !list.isEmpty() ? list : Lists.newArrayList();
    }

}
