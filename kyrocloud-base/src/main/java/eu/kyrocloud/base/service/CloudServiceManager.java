package eu.kyrocloud.base.service;

/*

  » eu.kyrocloud.launcher.service

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 21.03.2021 / 12:12

 */

import com.google.common.collect.Lists;
import eu.kyrocloud.api.CloudAPI;
import eu.kyrocloud.api.console.Colors;
import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.api.netty.NettyProcess;
import eu.kyrocloud.api.service.ServiceState;
import eu.kyrocloud.base.CloudBase;
import eu.kyrocloud.base.group.CloudGroupService;
import eu.kyrocloud.launcher.CloudBootstrap;
import eu.kyrocloud.base.netty.packets.abstracts.ServerPacket;
import io.netty.channel.Channel;

import java.util.List;

public class CloudServiceManager {

    private final List<CloudService> cloudServices = Lists.newArrayList();

    public CloudServiceManager(){
        addAllServices();
    }

    public void addAllServices(){
        for (CloudGroupService cloudGroupService : CloudBase.getInstance().getCloudGroupServiceManager().getGroupServices()) {
            for(int i = 0; i < cloudGroupService.getMaxServers(); i++){
                addService(i + 1, cloudGroupService);
            }
        }
    }

    public void addService(int serviceId, CloudGroupService cloudGroupService){
        CloudService cloudService = new CloudService(cloudGroupService.getName(), serviceId, cloudGroupService.getGroupType(), cloudGroupService.getGroupVersion());
        cloudServices.add(cloudService);
        cloudService.handleStart();
    }

    public void processBuild(NettyProcess nettyProcess, Object[] value, Channel channel){

        CloudService cloudService = CloudBase.getInstance().getCloudServiceManager().getCloudServices().stream().filter(service -> service.getServiceIdName().equals(value[1].toString().replaceAll(" ", ""))).findAny().orElse(null);

        if(cloudService == null){
            channel.close();
            return;
        }

        switch (nettyProcess){

            case SERVER_REGISTERED:

                cloudService.setChannel(channel);

                CloudBootstrap.getInstance().getLogger().write(LogType.INFO, "Cloud service " + Colors.GREEN + value[1] + Colors.RESET + " is connected. Took " + Colors.GREEN + (System.currentTimeMillis() - cloudService.getStartTime()) + "ms");

                cloudService.setServiceState(ServiceState.STARTED);
                cloudService.setAuthenticated(true);

                ServerPacket serverPacket = CloudBase.getInstance().getPacketHandler().getPacketByName("serverAddPacket");
                serverPacket.write(cloudService, "Hallo");

                break;

        }

    }

    public List<CloudService> getCloudServices() {
        return cloudServices;
    }
}
