package eu.kyrocloud.base.service;

/*

  » eu.kyrocloud.launcher.service

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 21.03.2021 / 11:28

 */

import com.google.common.collect.Lists;
import eu.kyrocloud.api.console.Colors;
import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.api.group.GroupType;
import eu.kyrocloud.api.group.GroupVersion;
import eu.kyrocloud.api.process.IServerProcessManager;
import eu.kyrocloud.api.service.ICloudService;
import eu.kyrocloud.api.service.ServiceState;
import eu.kyrocloud.jsonlib.JsonLib;
import eu.kyrocloud.launcher.CloudBootstrap;
import eu.kyrocloud.launcher.files.FileHandler;
import eu.kyrocloud.base.wrapper.process.CloudServiceProcessManager;
import io.netty.channel.Channel;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Random;

public class CloudService implements ICloudService {

    private String name;
    private int serviceId, onlinePlayers, port;
    private ServiceState serviceState;
    private long startTime;
    private GroupType groupType;
    private GroupVersion groupVersion;
    private CloudServiceProcessManager processManager;
    private boolean authenticated;
    private List<String> cachedScreenMessages;
    private Channel channel;

    public CloudService(String name, int serviceId, GroupType groupType, GroupVersion groupVersion) {
        setPort(randInt(10000, 50000));
        this.name = name;
        this.serviceId = serviceId;
        this.onlinePlayers = 0;
        this.groupType = groupType;
        this.groupVersion = groupVersion;
        this.serviceState = ServiceState.PREPARING;
        this.startTime = System.currentTimeMillis();
        this.cachedScreenMessages = Lists.newArrayList();
        this.processManager = new CloudServiceProcessManager(this);
    }

    public void handleStart() {
        CloudBootstrap.getInstance().getLogger().write(LogType.INFO, "Cloud service " + Colors.GREEN + getServiceIdName() + Colors.RESET + " is starting!");
        FileHandler fileHandler = CloudBootstrap.getInstance().getFileHandler();
        if (fileHandler.folderExist("temp/" + getServiceIdName()))
            fileHandler.deleteFiles(new File("temp/" + getServiceIdName()));
        fileHandler.createFolder("temp/" + getServiceIdName() + "/plugins");
        fileHandler.createFolder("temp/" + getServiceIdName() + "/modules");
        CloudBootstrap.getInstance().getModuleManager().getModules().forEach(module -> {
            fileHandler.copyFile("modules/" + module.getName() + ".jar", "temp/" + getServiceIdName() + "/modules/" + module.getName() + ".jar");
        });
        fileHandler.copyFile("storage/kyrocloud-plugin-1.0-SNAPSHOT-shaded.jar", "temp/" + getServiceIdName() + "/plugins/kyrocloud-plugin-1.0-SNAPSHOT-shaded.jar");
        fileHandler.copyFile("storage/" + getGroupVersion().getDisplay() + ".jar", "temp/" + getServiceIdName() + "/" + getGroupVersion().getDisplay() + ".jar");

        fileHandler.createFolder("temp/" + getServiceIdName() + "/KYROCLOUD");
        fileHandler.createFile("temp/" + getServiceIdName() + "/KYROCLOUD", "serviceInfo.json");

        try {
            FileWriter fileWriter = new FileWriter(new File("temp/" + getServiceIdName() + "/KYROCLOUD", "serviceInfo.json"));
            fileWriter.write(new JsonLib()
                    .append("serviceName", getServiceIdName())
                    .append("groupType", getGroupType().getDisplay())
                    .append("groupVersion", getGroupVersion().getDisplay()).appendAll());
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        processManager.start();

    }

    public void setServiceState(ServiceState serviceState) {
        this.serviceState = serviceState;
    }

    public int randInt(int min, int max) {
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }

    public void stop(boolean value) {
        if (channel != null) {

            if(value){
                switch (getGroupVersion()){
                    case BUNGEECORD: case WATERFALL:
                        processManager.executeCommand("end");
                        break;
                    case VELOCITY:

                        break;

                    default:
                        processManager.executeCommand("stop");
                        break;
                }

                processManager.stop();

            }


            CloudBootstrap.getInstance().getLogger().write(LogType.INFO, "Cloud service " + Colors.GREEN + getServiceIdName() + Colors.RESET + " was stopped");



            //TODO: autostart

        }
    }

    public String getServiceStateString() {
        if (serviceState.equals(ServiceState.PREPARING)) {
            return "" + Colors.BOLD_YELLOW + ServiceState.STARTED;
        } else if (serviceState.equals(ServiceState.STARTING)) {
            return "" + Colors.BOLD_CYAN + ServiceState.STARTING;
        } else if (serviceState.equals(ServiceState.STARTED)) {
            return "" + Colors.BOLD_GREEN + ServiceState.STARTED;
        } else if (serviceState.equals(ServiceState.CLOSED)) {
            return "" + Colors.BOLD_RED + ServiceState.CLOSED;
        }
        return "";
    }

    public String getServiceIdName(){
        return getName() + "-" + getServiceId();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public int getOnlinePlayers() {
        return onlinePlayers;
    }

    public void setOnlinePlayers(int onlinePlayers) {
        this.onlinePlayers = onlinePlayers;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public ServiceState getServiceState() {
        return serviceState;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public GroupType getGroupType() {
        return groupType;
    }

    public void setGroupType(GroupType groupType) {
        this.groupType = groupType;
    }

    public GroupVersion getGroupVersion() {
        return groupVersion;
    }

    public void setGroupVersion(GroupVersion groupVersion) {
        this.groupVersion = groupVersion;
    }

    public CloudServiceProcessManager getProcessManager() {
        return processManager;
    }

    public void setProcessManager(CloudServiceProcessManager processManager) {
        this.processManager = processManager;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }

    public List<String> getCachedScreenMessages() {
        return cachedScreenMessages;
    }

    public void setCachedScreenMessages(List<String> cachedScreenMessages) {
        this.cachedScreenMessages = cachedScreenMessages;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }
}
