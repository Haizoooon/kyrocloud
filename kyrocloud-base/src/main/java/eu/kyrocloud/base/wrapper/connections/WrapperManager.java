package eu.kyrocloud.base.wrapper.connections;

/*

  » eu.kyrocloud.base.wrapper.manager

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 24.03.2021 / 11:35

 */

import com.google.common.collect.Lists;
import eu.kyrocloud.api.wrapper.IWrapperManager;

import java.util.List;

public class WrapperManager implements IWrapperManager {

    private final List<Wrapper> wrappers = Lists.newArrayList();

    public WrapperManager() {
        wrappers.add(new Wrapper("127.0.0.1", "MainWrapper"));
        start();
    }

    @Override
    public void start() {
        for(Wrapper wrapper : wrappers){
            wrapper.connect();
        }
    }

    @Override
    public Wrapper getWrapperByName(String name) {
        return wrappers.stream().filter(wrapper -> wrapper.getName().equalsIgnoreCase(name)).findAny().orElse(null);
    }
}
