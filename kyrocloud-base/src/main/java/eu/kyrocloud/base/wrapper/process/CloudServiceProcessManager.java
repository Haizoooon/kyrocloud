package eu.kyrocloud.base.wrapper.process;

/*

  » eu.kyrocloud.base.wrapper

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 22.03.2021 / 13:57

 */

import com.google.common.collect.Lists;
import eu.kyrocloud.api.group.GroupVersion;
import eu.kyrocloud.api.service.ServiceState;
import eu.kyrocloud.base.CloudBase;
import eu.kyrocloud.base.group.CloudGroupService;
import eu.kyrocloud.base.service.CloudService;
import eu.kyrocloud.base.wrapper.process.configuration.service.DefaultSpigotConfiguration;
import eu.kyrocloud.launcher.CloudBootstrap;
import eu.kyrocloud.base.wrapper.process.configuration.service.DefaultBungeeConfiguration;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class CloudServiceProcessManager {

    private Process process;
    private Thread thread;

    private final CloudService cloudService;

    public CloudServiceProcessManager(CloudService cloudService){
        this.cloudService = cloudService;
    }

    public void start(){
        try {

            cloudService.setServiceState(ServiceState.STARTING);

            if(!CloudBootstrap.getInstance().getFileHandler().folderExist("temp/" + cloudService.getServiceIdName())) CloudBootstrap.getInstance().getFileHandler().createFolder("temp/" + cloudService.getServiceIdName());

            if (cloudService.getGroupVersion().equals(GroupVersion.BUNGEECORD) || cloudService.getGroupVersion().equals(GroupVersion.WATERFALL)) {
                DefaultBungeeConfiguration configuration = new DefaultBungeeConfiguration();
                configuration.configure(cloudService, new File("temp/" + cloudService.getServiceIdName()));
            } else if (cloudService.getGroupVersion().equals(GroupVersion.VELOCITY)) {

            } else {
                DefaultSpigotConfiguration defaultSpigotConfiguration = new DefaultSpigotConfiguration();
                defaultSpigotConfiguration.configure(cloudService, new File("temp/" + cloudService.getServiceIdName()));
            }

            this.process = createProcessBuilder().directory(new File("temp/" + cloudService.getServiceIdName())).start();

            this.thread = new Thread(() -> {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(this.process.getInputStream()));

                while(this.process.isAlive()){
                    try {
                        String string = bufferedReader.readLine();

                        if(string.isEmpty()) {
                            return;
                        }

                        if(!string.equals(" ") && !string.equals(">") && !string.equals(" >")) {
                            this.cloudService.getCachedScreenMessages().add(string);
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    bufferedReader.close();
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
                stop();
            });

            this.thread.start();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void stop(){
        deleteTempFiles();
        this.cloudService.setAuthenticated(false);
        this.cloudService.setOnlinePlayers(0);
        this.cloudService.setServiceState(ServiceState.CLOSED);
        this.cloudService.getCachedScreenMessages().clear();
        CloudBase.getInstance().getCloudServiceManager().getCloudServices().remove(cloudService);
    }

    public void deleteTempFiles(){
        while (true) {

            CloudBootstrap.getInstance().getFileHandler().deleteFiles(new File("temp/" + this.cloudService.getServiceIdName()));

            break;
        }
    }

    public List<String> getStartArguments(){

        List<String> arguments = Lists.newArrayList();
        CloudGroupService cloudGroupService = CloudBase.getInstance().getCloudGroupServiceManager().getGroupServices().stream().filter(cloudGroup -> cloudGroup.getName().equalsIgnoreCase(cloudService.getName())).findAny().orElse(null);

        assert cloudGroupService != null;

        arguments.add("java");
        arguments.add("-Dcom.mojang.eula.agree=true");
        arguments.add("-Djline.terminal=jline.UnsupportedTerminal");
        arguments.add("-Xms" + cloudGroupService.getMaxMemory() + "m");
        arguments.add("-Xmx" + cloudGroupService.getMaxMemory() + "m");
        arguments.add("-jar");
        arguments.add(cloudService.getGroupVersion().getDisplay() + ".jar");

        return arguments;

    }

    public void executeCommand(String command){
        String cmd = command + "\n";
        try {
            if(process != null && process.getOutputStream() != null){
                process.getOutputStream().write(cmd.getBytes());
                process.getOutputStream().flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ProcessBuilder createProcessBuilder(){
        return new ProcessBuilder(getStartArguments());
    }

    public boolean isActive(){
        return process.isAlive();
    }

}
