package eu.kyrocloud.base.wrapper.process.configuration;

/*

  » eu.kyrocloud.base.wrapper.process.configuration

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 22.03.2021 / 14:06

 */

import eu.kyrocloud.base.service.CloudService;

import java.io.File;

public interface IServiceConfiguration {

    void configure(CloudService cloudService, File file);

}
