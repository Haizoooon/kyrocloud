package eu.kyrocloud.base.wrapper.process.configuration.service;

/*

  » eu.kyrocloud.base.wrapper.process.configuration.service

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 22.03.2021 / 14:08

 */

import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.base.CloudBase;
import eu.kyrocloud.base.files.FileEditor;
import eu.kyrocloud.base.group.CloudGroupService;
import eu.kyrocloud.base.service.CloudService;
import eu.kyrocloud.base.wrapper.process.configuration.IServiceConfiguration;
import eu.kyrocloud.launcher.CloudBootstrap;

import java.io.File;

public class DefaultSpigotConfiguration implements IServiceConfiguration {
    @Override
    public void configure(CloudService cloudService, File file) {
        File propertiesFile = new File(file, "server.properties");
        if(!propertiesFile.exists()){
            CloudBootstrap.getInstance().getFileHandler().copyFileOutOfJar(propertiesFile, "/files/server.properties");
        }
        CloudGroupService cloudGroupService = CloudBase.getInstance().getCloudGroupServiceManager().getGroupServices().stream().filter(cloudGroup -> cloudGroup.getName().equalsIgnoreCase(cloudService.getName())).findAny().orElse(null);

        FileEditor fileEditor = new FileEditor(propertiesFile);
        fileEditor.replaceLine("server-ip=127.0.0.1", "server-ip=127.0.0.1");
        fileEditor.replaceLine("server-port=25565", "server-port=" + cloudService.getPort());
        fileEditor.replaceLine("max-players=20", "max-players=" + cloudGroupService.getMaxPlayers());
        fileEditor.save();
        CloudBootstrap.getInstance().getLogger().write(LogType.DEBUG, "Spigot config");
    }
}
