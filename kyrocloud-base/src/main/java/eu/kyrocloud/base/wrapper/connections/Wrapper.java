package eu.kyrocloud.base.wrapper.connections;

/*

  » eu.kyrocloud.base.wrapper.manager

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 24.03.2021 / 11:37

 */

import eu.kyrocloud.api.console.Colors;
import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.api.wrapper.IWrapper;
import eu.kyrocloud.launcher.CloudBootstrap;

public class Wrapper extends IWrapper {

    public Wrapper(String address, String name) {
        super(address, name);
    }

    @Override
    public void connect() {
        CloudBootstrap.getInstance().getLogger().write(LogType.INFO, "Wrapper " + Colors.RED + getName() + Colors.RESET + " was connected!");
    }

}
