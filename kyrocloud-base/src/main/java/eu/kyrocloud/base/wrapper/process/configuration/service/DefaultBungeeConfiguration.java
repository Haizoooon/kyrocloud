package eu.kyrocloud.base.wrapper.process.configuration.service;

/*

  » eu.kyrocloud.base.wrapper.process.configuration.service

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 22.03.2021 / 14:08

 */

import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.api.group.GroupType;
import eu.kyrocloud.base.CloudBase;
import eu.kyrocloud.base.files.FileEditor;
import eu.kyrocloud.base.group.CloudGroupService;
import eu.kyrocloud.base.service.CloudService;
import eu.kyrocloud.launcher.CloudBootstrap;
import eu.kyrocloud.base.wrapper.process.configuration.IServiceConfiguration;

import java.io.File;

public class DefaultBungeeConfiguration implements IServiceConfiguration {
    @Override
    public void configure(CloudService cloudService, File file) {
        File newFile = new File(file, "config.yml");
        if(!newFile.exists()){
            CloudBootstrap.getInstance().getFileHandler().copyFileOutOfJar(newFile, "/files/config.yml");
        }
        CloudGroupService cloudGroupService = CloudBase.getInstance().getCloudGroupServiceManager().getGroupServices().stream().filter(cloudGroup -> cloudGroup.getName().equalsIgnoreCase(cloudService.getName())).findAny().orElse(null);

        CloudGroupService lobbyService = CloudBase.getInstance().getCloudGroupServiceManager().getGroupServices().stream().filter(cloudGroupServices -> cloudGroupServices.getGroupType().equals(GroupType.LOBBY)).findAny().orElse(null);

        FileEditor fileEditor = new FileEditor(newFile);
        fileEditor.replaceLine("  host: 0.0.0.0:25577", String.valueOf(cloudService.getPort()));
        fileEditor.replaceLine("  max-players: 1", String.valueOf(cloudGroupService.getMaxPlayers()));
        fileEditor.save();
    }
}
