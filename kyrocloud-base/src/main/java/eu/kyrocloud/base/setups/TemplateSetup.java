package eu.kyrocloud.base.setups;

import eu.kyrocloud.api.console.Colors;
import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.base.CloudBase;
import eu.kyrocloud.launcher.CloudBootstrap;
import eu.kyrocloud.launcher.setup.interfaces.ISetup;
import eu.kyrocloud.launcher.setup.types.SetupEnd;
import eu.kyrocloud.launcher.setup.types.SetupInput;

import java.util.List;

public class TemplateSetup implements ISetup {

    private String name;

    public TemplateSetup() {
        CloudBootstrap.getInstance().getSetupBuilder().register(this, new SetupEnd() {
            @Override
            public void handleEnd() {

                CloudBase.getInstance().getTemplateManager().createTemplate(name);
                CloudBootstrap.getInstance().getLogger().write(LogType.INFO, "The template " + Colors.GREEN + name + Colors.RESET + " has been created");

            }
        }, new SetupInput("Please provide the name of the template") {
            @Override
            public List<String> getSuggestions() {
                return null;
            }

            @Override
            public boolean handle(String input) {
                name = input;
                return true;
            }
        });
    }
}
