package eu.kyrocloud.base.setups;

import com.google.common.collect.Lists;
import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.api.group.GroupType;
import eu.kyrocloud.api.group.GroupVersion;
import eu.kyrocloud.api.template.ITemplate;
import eu.kyrocloud.base.CloudBase;
import eu.kyrocloud.launcher.CloudBootstrap;
import eu.kyrocloud.launcher.console.ConsoleSender;
import eu.kyrocloud.launcher.console.logging.LoggerProvider;
import eu.kyrocloud.launcher.setup.interfaces.ISetup;
import eu.kyrocloud.launcher.setup.types.SetupEnd;
import eu.kyrocloud.launcher.setup.types.SetupInput;

import java.util.Arrays;
import java.util.List;

public class GroupSetup implements ISetup {

    private String name, template, minServers, maxServers, maxMemory, percentageToStart, maxPlayers, groupType, groupVersion;

    public GroupSetup() {
        CloudBootstrap.getInstance().getSetupBuilder().register(this, new SetupEnd() {
            @Override
            public void handleEnd() {

                LoggerProvider loggerProvider = CloudBootstrap.getInstance().getLogger();
                loggerProvider.write(LogType.DEBUG, name);
                loggerProvider.write(LogType.DEBUG, template);
                loggerProvider.write(LogType.DEBUG, minServers);
                loggerProvider.write(LogType.DEBUG, maxServers);
                loggerProvider.write(LogType.DEBUG, maxMemory);
                loggerProvider.write(LogType.DEBUG, percentageToStart);
                loggerProvider.write(LogType.DEBUG, maxPlayers);
                loggerProvider.write(LogType.DEBUG, groupType);
                loggerProvider.write(LogType.DEBUG, groupVersion);

            }
        }, new SetupInput("Please provide the name of the group") {
            @Override
            public List<String> getSuggestions() {
                return null;
            }

            @Override
            public boolean handle(String input) {
                name = input;
                return true;
            }
        }, new SetupInput("Please provide the minimum servers") {
            @Override
            public List<String> getSuggestions() {
                return null;
            }

            @Override
            public boolean handle(String input) {
                minServers = input;
                return isNum(input);
            }
        }, new SetupInput("Please provide the maximum servers") {
            @Override
            public List<String> getSuggestions() {
                return null;
            }

            @Override
            public boolean handle(String input) {
                maxServers = input;
                return isNum(input);
            }
        }, new SetupInput("Please provide the maximum memory") {
            @Override
            public List<String> getSuggestions() {
                return null;
            }

            @Override
            public boolean handle(String input) {
                maxMemory = input;
                return isNum(input);
            }
        }, new SetupInput("Please provide the percentage to start a new service") {
            @Override
            public List<String> getSuggestions() {
                return null;
            }

            @Override
            public boolean handle(String input) {
                percentageToStart = input;
                return isPercent(input);
            }
        }, new SetupInput("Please provide the maximum players that can join the service") {
            @Override
            public List<String> getSuggestions() {
                return null;
            }

            @Override
            public boolean handle(String input) {
                maxPlayers = input;
                return isNum(input);
            }
        }, new SetupInput("Please provide the template for the group") {
            @Override
            public List<String> getSuggestions() {
                List<String> templates = Lists.newArrayList();
                for (ITemplate template : CloudBase.getInstance().getTemplateManager().getAllTemplates()) {
                    templates.add(template.getName());
                }
                return templates;
            }

            @Override
            public boolean handle(String input) {
                template = input.replace(" ", "");
                return getSuggestions().contains(template);
            }
        }, new SetupInput("Please provide the group Type") {
            @Override
            public List<String> getSuggestions() {
                List<String> types = Lists.newArrayList();
                for (GroupType groupType : GroupType.values()) {
                    types.add(groupType.getDisplay());
                }
                return types;
            }

            @Override
            public boolean handle(String input) {
                groupType = input.replace(" ", "");
                return getSuggestions().contains(groupType);
            }
        }, new SetupInput("Please provide the group Version") {
            @Override
            public List<String> getSuggestions() {
                List<String> groupVersions = Lists.newArrayList();
                for(GroupVersion groupVersion : GroupVersion.values()){
                    if (groupVersion.getGroupType().equals(Arrays.stream(GroupType.values()).filter(groupType1 -> groupType1.getDisplay().equalsIgnoreCase(groupType)).findAny().orElse(null))) {
                        groupVersions.add(groupVersion.getDisplay());
                    }
                }
                return groupVersions;
            }

            @Override
            public boolean handle(String input) {
                groupVersion = input.replace(" ", "");
                return getSuggestions().contains(groupVersion);
            }
        });
    }

    private boolean isNum(String input) {
        return input.matches("[0-9]+");
    }
    private boolean isPercent(String input) {
        return Integer.parseInt(input) <= 100;
    }

}
