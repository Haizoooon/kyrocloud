package eu.kyrocloud.base;

/*

  » eu.kyrocloud.base

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 22.03.2021 / 22:06

 */

import com.google.common.collect.Lists;
import eu.kyrocloud.api.CloudAPI;
import eu.kyrocloud.api.commands.IConsoleCommand;
import eu.kyrocloud.base.commands.*;
import eu.kyrocloud.base.event.manager.EventManager;
import eu.kyrocloud.base.group.CloudGroupServiceManager;
import eu.kyrocloud.base.netty.NettyServerConnector;
import eu.kyrocloud.base.netty.packets.handler.PacketHandler;
import eu.kyrocloud.base.netty.packets.packet.ServerAddPacket;
import eu.kyrocloud.base.service.CloudServiceManager;
import eu.kyrocloud.base.template.TemplateManager;
import eu.kyrocloud.base.wrapper.connections.WrapperManager;
import eu.kyrocloud.launcher.CloudBootstrap;

import java.util.List;

public class CloudBase {

    private final TemplateManager templateManager;
    private final CloudGroupServiceManager cloudGroupServiceManager;
    private final CloudServiceManager cloudServiceManager;
    private final NettyServerConnector nettyServerConnector;
    private final PacketHandler packetHandler;
    private final WrapperManager wrapperManager;
    private final EventManager eventManager;

    private static CloudBase instance;

    public static void main(String[] args) {
        new CloudBase();
    }

    public CloudBase(){
        instance = this;

        new CloudBootstrap();

        new ExecuteCommand();
        new ShutdownCommand();
        new InfoCommand();
        new ScreenCommand();
        new GroupCommand();
        new StopCommand();
        new CreateCommand();

        this.packetHandler = new PacketHandler();
        this.nettyServerConnector = new NettyServerConnector(5499).listening();
        this.wrapperManager = new WrapperManager();
        this.templateManager = new TemplateManager();
        this.cloudGroupServiceManager = new CloudGroupServiceManager();
        this.cloudServiceManager = new CloudServiceManager();
        this.eventManager = new EventManager();

        this.packetHandler.registerPacket(new ServerAddPacket());

        List<String> suggestions = Lists.newArrayList();

        for(IConsoleCommand command : IConsoleCommand.getCommands()){
            String commandName = command.getCommand();
            suggestions.add(commandName);
        }

        CloudBootstrap.getInstance().getConsoleManager().getConsoleCompleter().setSuggestions(suggestions);

    }

    public EventManager getEventManager() {
        return eventManager;
    }

    public WrapperManager getWrapperManager() {
        return wrapperManager;
    }

    public NettyServerConnector getNettyServerConnector() {
        return nettyServerConnector;
    }

    public PacketHandler getPacketHandler() {
        return packetHandler;
    }

    public TemplateManager getTemplateManager() {
        return templateManager;
    }

    public CloudGroupServiceManager getCloudGroupServiceManager() {
        return cloudGroupServiceManager;
    }

    public CloudServiceManager getCloudServiceManager() {
        return cloudServiceManager;
    }

    public static CloudBase getInstance() {
        return instance;
    }
}
