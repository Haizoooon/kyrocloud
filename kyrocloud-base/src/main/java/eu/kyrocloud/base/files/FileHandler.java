package eu.kyrocloud.base.files;

/*

  » eu.kyrocloud.launcher.files

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 21.03.2021 / 18:17

 */

import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.launcher.CloudBootstrap;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Objects;

public class FileHandler {

    public FileHandler() {
        deleteTempOrder();
    }

    public void deleteFiles(File folderName){
        if(folderName.exists()){
            for (File file : Objects.requireNonNull(folderName.listFiles())) {
                if(file.exists()){
                    if (file.isDirectory())
                        deleteFiles(file);
                    file.delete();
                }
            }
            folderName.delete();
        }
    }

    public void copyFileOutOfJar(File file, String name){
        InputStream inputStream = getClass().getResourceAsStream(name);
        File parent = file.getParentFile();
        parent.mkdirs();
        if(new File(name).exists()){
            return;
        }
        try {
            file.createNewFile();
            FileUtils.copyInputStreamToFile(inputStream, file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean createFile(String folderName, String fileName){
        try {
            return new File(folderName, fileName).createNewFile();
        } catch (IOException e) {
            CloudBootstrap.getInstance().getLogger().write(LogType.ERROR, "Cannot create file '" + fileName + "' in folder '" + folderName + "'!");
            return false;
        }
    }

    public boolean copyFile(String from, String to){
        try {
            Files.copy(new File(from).toPath(), new File(to).toPath());
            return true;
        } catch (IOException e) {
            CloudBootstrap.getInstance().getLogger().write(LogType.ERROR, "Cannot copy file '" + from + "' in '" + to + "'!");
            e.printStackTrace();
            return false;
        }
    }

    public boolean createFolder(String folderName){
        try {
            new File(folderName).mkdirs();
            return true;
        } catch (Exception e) {
            CloudBootstrap.getInstance().getLogger().write(LogType.ERROR, "Cannot create folder  '" +  folderName + "'!");
            return false;
        }
    }

    public boolean folderExist(String folderName){
        return new File(folderName).exists();
    }

    public boolean fileExist(String folderName, String fileName){
        try {
            return new File(folderName, fileName).exists();
        } catch (Exception e) {
            CloudBootstrap.getInstance().getLogger().write(LogType.ERROR, "Cannot check if file '" + fileName + "' in folder '" + folderName + "' is existing!");
            return false;
        }
    }

    public void createEula(String folder){
        File file = new File(folder, "eula.txt");
        try {
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write("eula=true\n");
            fileWriter.close();

        } catch (IOException ignored) { }
    }

    public void deleteTempOrder(){
        deleteFiles(new File("temp"));
    }

}
