package eu.kyrocloud.base.files;

/*

  » de.stickcloud.launcher.files

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 10.03.2021 / 10:35

 */

import org.json.JSONObject;

public class CloudJsonReader {

    private JSONObject jsonObject;

    public CloudJsonReader(String content) {
        this.jsonObject = new JSONObject(content);
    }

    public Object read(String var1){
        return jsonObject.get(var1);
    }

}
