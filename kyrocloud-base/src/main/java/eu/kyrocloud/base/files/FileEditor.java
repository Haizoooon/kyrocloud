package eu.kyrocloud.base.files;

/*

  » eu.kyrocloud.launcher.files

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 22.03.2021 / 14:09

 */

import com.google.common.collect.Lists;
import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.launcher.CloudBootstrap;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class FileEditor {

    private File file;
    private List<String> lines;

    public FileEditor(File file){
        this.file = file;
        lines = Lists.newArrayList();
        readFile();
    }

    public void readFile(){
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(file));
            String line;
            while((line = bufferedReader.readLine()) != null){
                lines.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(bufferedReader != null){
                try {
                    bufferedReader.close();
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
            }
        }
    }

    public void replaceLine(String line, String value){
        for(int i = 0; i < lines.size(); i++){
            String string = lines.get(i);
            if(string.equalsIgnoreCase(line)){
                lines.remove(i);
                lines.set(i, value);
                return;
            }
        }
    }

    public void save(){
        try {
            FileWriter fileWriter = new FileWriter(file);

            for(String line : lines){
                fileWriter.write(line + "\n\r");
            }

            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
