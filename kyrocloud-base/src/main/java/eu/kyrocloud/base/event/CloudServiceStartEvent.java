package eu.kyrocloud.base.event;

/*

  » eu.kyrocloud.base.events

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 25.03.2021 / 13:54

 */

import eu.kyrocloud.api.events.Event;
import eu.kyrocloud.api.events.EventInject;
import eu.kyrocloud.base.service.CloudService;

@EventInject(name = "cloudServiceStartEvent")
public class CloudServiceStartEvent implements Event {

    private CloudService cloudService;
    private boolean cancelled;

    public CloudServiceStartEvent(CloudService cloudService, boolean cancelled) {
        this.cloudService = cloudService;
        this.cancelled = cancelled;
    }

    public CloudService getCloudService() {
        return cloudService;
    }

    public void setCloudService(CloudService cloudService) {
        this.cloudService = cloudService;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}
