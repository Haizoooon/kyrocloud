package eu.kyrocloud.base.event.manager;

/*

  » eu.kyrocloud.base.event

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 25.03.2021 / 13:50

 */

import com.google.common.collect.Lists;
import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.api.events.Event;
import eu.kyrocloud.api.events.EventInject;
import eu.kyrocloud.launcher.CloudBootstrap;

import java.util.List;

public class EventManager {

    private List<Event> eventList;

    public EventManager() {
        this.eventList = Lists.newArrayList();
    }

    public void registerEvent(Event event){

        Class clazz = event.getClass();

        if(clazz.getAnnotation(EventInject.class) == null){
            return;
        }

        EventInject eventInject = (EventInject) clazz.getAnnotation(EventInject.class);

        CloudBootstrap.getInstance().getLogger().write(LogType.INFO, "Registered event " + eventInject.name());
        eventList.add(event);

    }

    public List<Event> getEventList() {
        return eventList;
    }
}
