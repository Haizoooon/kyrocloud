package eu.kyrocloud.base.event.listener;

/*

  » eu.kyrocloud.base.event.listener

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 25.03.2021 / 13:56

 */

import eu.kyrocloud.api.events.EventHandler;
import eu.kyrocloud.api.events.EventListener;
import eu.kyrocloud.base.event.CloudServiceStartEvent;
import eu.kyrocloud.base.service.CloudService;

public class CloudServiceStartListener implements EventListener {

    @EventHandler
    public void handle(CloudServiceStartEvent event){
        CloudService cloudService = event.getCloudService();

        //TODO: do something

    }

}
