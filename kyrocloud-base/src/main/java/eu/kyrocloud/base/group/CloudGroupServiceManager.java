package eu.kyrocloud.base.group;

/*

  » eu.kyrocloud.launcher.group

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 21.03.2021 / 12:12

 */

import com.google.common.collect.Lists;
import eu.kyrocloud.api.console.Colors;
import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.base.group.sql.CloudGroupSqlAdapter;
import eu.kyrocloud.launcher.CloudBootstrap;

import java.util.List;

public class CloudGroupServiceManager extends CloudGroupSqlAdapter {

    private final List<CloudGroupService> groupServices = Lists.newArrayList();

    public CloudGroupServiceManager() {
        groupServices.addAll(getAllGroupServices());
        CloudBootstrap.getInstance().getLogger().write(LogType.INFO, "Loaded following groups:");
        groupServices.forEach(groupService -> CloudBootstrap.getInstance().getLogger().write(LogType.INFO, "- " + Colors.GREEN + groupService.getName()));
    }

    public List<CloudGroupService> getGroupServices() {
        return groupServices;
    }
}
