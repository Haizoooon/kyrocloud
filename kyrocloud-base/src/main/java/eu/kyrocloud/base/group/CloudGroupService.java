package eu.kyrocloud.base.group;

/*

  » eu.kyrocloud.launcher.group

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 21.03.2021 / 11:32

 */

import eu.kyrocloud.api.group.GroupType;
import eu.kyrocloud.api.group.GroupVersion;

public class CloudGroupService {

    private final String name;
    private final int maxServers, minServers, maxMemory, percentageToStartNewService, maxPlayers;
    private final boolean maintenance;

    private final GroupType groupType;
    private final GroupVersion groupVersion;

    public CloudGroupService(String name, int maxServers, int minServers, int maxMemory, int percentageToStartNewService, int maxPlayers, boolean maintenance, GroupType groupType, GroupVersion groupVersion) {
        this.name = name;
        this.maxServers = maxServers;
        this.minServers = minServers;
        this.maxMemory = maxMemory;
        this.percentageToStartNewService = percentageToStartNewService;
        this.maxPlayers = maxPlayers;
        this.maintenance = maintenance;
        this.groupType = groupType;
        this.groupVersion = groupVersion;
    }

    public String getName() {
        return name;
    }

    public int getMaxServers() {
        return maxServers;
    }

    public int getMinServers() {
        return minServers;
    }

    public int getMaxMemory() {
        return maxMemory;
    }

    public int getPercentageToStartNewService() {
        return percentageToStartNewService;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public boolean isMaintenance() {
        return maintenance;
    }

    public GroupType getGroupType() {
        return groupType;
    }

    public GroupVersion getGroupVersion() {
        return groupVersion;
    }
}
