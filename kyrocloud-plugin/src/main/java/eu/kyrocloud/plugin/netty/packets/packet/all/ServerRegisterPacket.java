package eu.kyrocloud.plugin.netty.packets.packet.all;

/*

  » eu.kyrocloud.plugin.netty.packets.packet

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 25.03.2021 / 20:28

 */

import eu.kyrocloud.api.packet.ClientPacket;

public class ServerRegisterPacket extends ClientPacket {

    public ServerRegisterPacket() {
        super("serverRegisterPacket");
    }

    @Override
    public void read(String value) {

    }

    @Override
    public void write(String value) {

    }

}
