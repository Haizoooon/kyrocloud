package eu.kyrocloud.plugin.netty;

/*

  » netty

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 10.03.2021 / 13:42

 */

import io.netty.channel.Channel;

public class CommunicationHandler {

    private static CommunicationHandler instance;

    private final CloudNettyClient cloudNettyClient;
    private final String serviceName;

    private Channel serviceChannel;

    public CommunicationHandler(String serviceName) {
        instance = this;
        this.serviceName = serviceName;

        this.cloudNettyClient = new CloudNettyClient();
        this.cloudNettyClient.connect();
    }

    public CloudNettyClient getCloudNettyClient() {
        return cloudNettyClient;
    }

    public String getServiceName() {
        return serviceName;
    }

    public static CommunicationHandler getInstance() {
        return instance;
    }

    public Channel getServiceChannel(){
        return serviceChannel;
    }

    public void setServiceChannel(Channel serviceChannel) {
        this.serviceChannel = serviceChannel;
    }
}
