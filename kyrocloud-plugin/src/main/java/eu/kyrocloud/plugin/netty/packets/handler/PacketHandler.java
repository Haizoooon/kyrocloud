package eu.kyrocloud.plugin.netty.packets.handler;

/*

  » eu.kyrocloud.plugin.netty.packets.handler

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 21.03.2021 / 13:56

 */

import com.google.common.collect.Lists;
import eu.kyrocloud.api.packet.ClientPacket;
import eu.kyrocloud.api.packet.IPacketRegistry;

import java.util.ArrayList;

public class PacketHandler implements IPacketRegistry {

    private static final ArrayList<ClientPacket> packets = Lists.newArrayList();

    @Override
    public void registerPacket(ClientPacket packet) {
        packets.add(packet);
    }

    public ClientPacket getPacketByName(String name){
        return packets.stream().filter(packet -> packet.getPacketName().equalsIgnoreCase(name)).findAny().orElse(null);
    }

    public static ArrayList<ClientPacket> getPackets() {
        return packets;
    }
}
