package eu.kyrocloud.plugin.netty;

/*

  » netty

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 10.03.2021 / 13:41

 */

public class CloudNettyClient {

    private Thread thread;
    private NettyClientConnector nettyClientConnector;

    public void connect(){
        this.thread = new Thread(nettyClientConnector = new NettyClientConnector());
        this.thread.start();
    }

    public void disconnect(){
        this.nettyClientConnector.getGroup().shutdownGracefully();
        this.thread.stop();
    }

}
