package eu.kyrocloud.plugin.netty.packets.packet.bungee;

/*

  » eu.kyrocloud.plugin.netty.packets.packet

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 22.03.2021 / 21:00

 */

import eu.kyrocloud.api.CloudAPI;
import eu.kyrocloud.api.group.GroupType;
import eu.kyrocloud.api.group.GroupVersion;
import eu.kyrocloud.api.packet.ClientPacket;
import eu.kyrocloud.api.service.ICloudService;
import eu.kyrocloud.api.service.ServiceState;
import eu.kyrocloud.plugin.CloudPluginBootstrap;
import eu.kyrocloud.plugin.bootstrap.bungee.BungeeBootstrap;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import org.json.JSONObject;

import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.List;

public class ServerAddPacket extends ClientPacket {

    public ServerAddPacket() {
        super("serverAddPacket");
    }

    @Override
    public void read(String value) {

        JSONObject jsonObject = new JSONObject(value);
        String name = jsonObject.getString("serviceName");
        int port = jsonObject.getInt("port");
        GroupVersion groupVersion = Arrays.stream(GroupVersion.values()).filter(groupVersions -> groupVersions.getDisplay().equalsIgnoreCase(jsonObject.getString("groupVersion"))).findAny().orElse(null);
        GroupType groupType = Arrays.stream(GroupType.values()).filter(groupVersions -> groupVersions.getDisplay().equalsIgnoreCase(jsonObject.getString("groupType"))).findAny().orElse(null);
        ServiceState serviceState =  Arrays.stream(ServiceState.values()).filter(groupVersions -> groupVersions.getDisplay().equalsIgnoreCase(jsonObject.getString("serviceState"))).findAny().orElse(null);
        int serviceId = jsonObject.getInt("serviceId");

        assert groupType != null;
        if (groupType.equals(GroupType.PROXY)) {
            return;
        }

        BungeeBootstrap.getBootstrap().registerService(name + "-" + serviceId, new InetSocketAddress("127.0.0.1", port));
        ProxyServer.getInstance().broadcast(new TextComponent("§8» §f§lKyro§c§lCloud §8× §7Der Service §f§l" + name + "-" + serviceId + " §7wurde §a§lgestartet§8!"));

    }

    @Override
    public void write(String value) {

    }
}
