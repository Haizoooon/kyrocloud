package eu.kyrocloud.plugin.netty;

/*

  » netty

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 10.03.2021 / 13:40

 */

import eu.kyrocloud.api.netty.NettyProcess;
import eu.kyrocloud.api.packet.ClientPacket;
import eu.kyrocloud.plugin.CloudPluginBootstrap;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.Arrays;

public class CloudChannelReader extends SimpleChannelInboundHandler<String> {

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, String msg) {

        String[] args = msg.split(";");
        String type = args[0];
        NettyProcess nettyProcess = Arrays.stream(NettyProcess.values()).filter(process -> process.toString().equals(type)).findAny().orElse(null);

        if(nettyProcess == null){
            return;
        }

        if (nettyProcess.equals(NettyProcess.SERVER_PROXY_ADD)) {
            ClientPacket clientPacket = CloudPluginBootstrap.getInstance().getPacketHandler().getPacketByName("serverAddPacket");
            if(clientPacket != null){
                clientPacket.read(args[1]);
            } else {
                System.out.println("Error while reading packet...");
            }
        }

    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        CommunicationHandler.getInstance().setServiceChannel(ctx.channel());
        ctx.channel().writeAndFlush("#" + NettyProcess.SERVER_REGISTERED + ";" + CommunicationHandler.getInstance().getServiceName() + "\n");
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) {
        
    }
}
