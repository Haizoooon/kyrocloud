package eu.kyrocloud.plugin.netty;

/*

  » netty

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 10.03.2021 / 13:36

 */

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

public class NettyClientConnector implements Runnable {

    private EventLoopGroup group;
    private Channel channel;

    @Override
    public void run() {
        group = new NioEventLoopGroup();
        try {

            Bootstrap bootstrap = new Bootstrap().group(group).channel(NioSocketChannel.class).handler(new CloudChannelInitializer());
            channel = bootstrap.connect("127.0.0.1", 5499).sync().channel();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public EventLoopGroup getGroup() {
        return group;
    }

    public Channel getChannel() {
        return channel;
    }
}
