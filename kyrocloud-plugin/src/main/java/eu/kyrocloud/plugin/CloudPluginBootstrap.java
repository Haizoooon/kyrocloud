package eu.kyrocloud.plugin;

/*

  » eu.kyrocloud.plugin

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 19.03.2021 / 21:53

 */

import eu.kyrocloud.plugin.bootstrap.bungee.BungeeBootstrap;
import eu.kyrocloud.plugin.bootstrap.spigot.SpigotBootstrap;
import eu.kyrocloud.plugin.info.CloudServiceInfoHandler;
import eu.kyrocloud.plugin.netty.packets.handler.PacketHandler;
import eu.kyrocloud.plugin.netty.packets.packet.bungee.ServerAddPacket;

public class CloudPluginBootstrap {

    private static CloudPluginBootstrap instance;
    private final CloudServiceInfoHandler cloudServiceInfoHandler;
    private final PacketHandler packetHandler;

    public CloudPluginBootstrap(){
        instance = this;

        this.cloudServiceInfoHandler = new CloudServiceInfoHandler();
        this.packetHandler = new PacketHandler();

        switch (this.cloudServiceInfoHandler.getGroupVersion()){
            case BUNGEECORD: case WATERFALL:
                BungeeBootstrap.getBootstrap().register();
                break;

            default:
                SpigotBootstrap.getBootstrap().register();
                break;

        }


        this.packetHandler.registerPacket(new ServerAddPacket());

    }

    public static CloudPluginBootstrap getInstance() {
        return instance;
    }

    public CloudServiceInfoHandler getCloudServiceInfoHandler() {
        return cloudServiceInfoHandler;
    }

    public PacketHandler getPacketHandler() {
        return packetHandler;
    }
}
