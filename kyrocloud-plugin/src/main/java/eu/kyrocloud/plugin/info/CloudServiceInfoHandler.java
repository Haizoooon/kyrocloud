package eu.kyrocloud.plugin.info;

/*

  » eu.kyrocloud.plugin.info

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 21.03.2021 / 13:50

 */

import eu.kyrocloud.api.group.GroupType;
import eu.kyrocloud.api.group.GroupVersion;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

public class CloudServiceInfoHandler {

    private String serviceName;
    private GroupVersion groupVersion;
    private GroupType groupType;

    public CloudServiceInfoHandler() {
        register();
    }

    public void register(){
        try {
            File file = new File("KYROCLOUD", "serviceInfo.json");

            String content = new String(Files.readAllBytes(Paths.get(file.toURI())), StandardCharsets.UTF_8);

            JSONObject jsonObject = new JSONObject(content);

            serviceName = jsonObject.getString("serviceName");
            groupVersion = Arrays.stream(GroupVersion.values()).filter(version -> version.getDisplay().equalsIgnoreCase(jsonObject.getString("groupVersion"))).findAny().orElse(null);
            groupType = Arrays.stream(GroupType.values()).filter(type -> type.getDisplay().equalsIgnoreCase(jsonObject.getString("groupType"))).findAny().orElse(null);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getServiceName() {
        return serviceName;
    }

    public GroupVersion getGroupVersion() {
        return groupVersion;
    }

    public GroupType getGroupType() {
        return groupType;
    }
}
