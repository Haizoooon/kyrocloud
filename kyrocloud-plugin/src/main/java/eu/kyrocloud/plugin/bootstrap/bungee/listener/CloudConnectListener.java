package eu.kyrocloud.plugin.bootstrap.bungee.listener;

/*

  » eu.kyrocloud.plugin.bootstrap.bungee.listener

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 21.03.2021 / 13:35

 */

import eu.kyrocloud.api.console.Colors;
import eu.kyrocloud.api.netty.NettyProcess;
import eu.kyrocloud.plugin.netty.CommunicationHandler;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class CloudConnectListener implements Listener {

    @EventHandler
    public void handle(PostLoginEvent event){
        event.getPlayer().setReconnectServer(null);
        if(ProxyServer.getInstance().getServerInfo("Lobby-1") == null){
            event.getPlayer().disconnect(new TextComponent("§cService Lobby cannot be found!"));
            return;
        }
    }

    @EventHandler
    public void handle(ServerConnectEvent event){
        event.setTarget(ProxyServer.getInstance().getServerInfo("Lobby-1"));
        System.out.println(event.getTarget().getName());
    }

    @EventHandler
    public void handle(LoginEvent event){
        String message = "Player " + event.getConnection().getName() + " connected. (" + event.getConnection().getUniqueId().toString() + "/" + event.getConnection().getAddress().getAddress().getHostAddress() + "/" + CommunicationHandler.getInstance().getServiceName() +  ")" + "\n";
        CommunicationHandler.getInstance().getServiceChannel().writeAndFlush("#" + NettyProcess.SERVER_PROXY_PLAYER_CONNECTED + ";" + CommunicationHandler.getInstance().getServiceName() + ";" + message);
    }

    @EventHandler
    public void handle(PlayerDisconnectEvent event){
        ProxiedPlayer proxiedPlayer = event.getPlayer();
        CommunicationHandler.getInstance().getServiceChannel().writeAndFlush("#" + NettyProcess.SERVER_PROXY_PLAYER_DISCONNECTED + ";" + CommunicationHandler.getInstance().getServiceName() + ";" + "Player " + Colors.GREEN + proxiedPlayer.getName() + Colors.RESET + " disconnected. (" + proxiedPlayer.getPendingConnection().getAddress().getAddress().getHostAddress() + ")" + "\n\r");
    }

}
