package eu.kyrocloud.plugin.bootstrap.bungee;

/*

  » eu.kyrocloud.plugin.bootstrap.bungee

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 21.03.2021 / 13:34

 */

import eu.kyrocloud.plugin.CloudPluginBootstrap;
import eu.kyrocloud.plugin.bootstrap.bungee.listener.CloudConnectListener;
import eu.kyrocloud.plugin.netty.CommunicationHandler;
import eu.kyrocloud.plugin.reconnect.ReconnectHandlerImpl;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ListenerInfo;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.plugin.Plugin;

import java.net.InetSocketAddress;
import java.util.UUID;

public class BungeeBootstrap extends Plugin {

    private static BungeeBootstrap bootstrap;
    private CommunicationHandler communicationHandler;


    public void onLoad() {
        ProxyServer.getInstance().setReconnectHandler(new ReconnectHandlerImpl());
    }

    public void onEnable() {
        bootstrap = this;
        new CloudPluginBootstrap();

        ProxyServer.getInstance().getConfigurationAdapter().getServers().clear();
        ProxyServer.getInstance().getServers().clear();

        for(ListenerInfo listenerInfo : ProxyServer.getInstance().getConfigurationAdapter().getListeners()){
            listenerInfo.getServerPriority().clear();
        }
        registerFallback();

    }

    public void registerFallback(){
        registerService("fallback", new InetSocketAddress("127.0.0.1", 0));
    }

    public void registerService(String name, InetSocketAddress inetSocketAddress){
        ServerInfo serverInfo = ProxyServer.getInstance().constructServerInfo(name, inetSocketAddress, "A kyrocloud service", false);
        ProxyServer.getInstance().getServers().put(name, serverInfo);
    }

    public void register(){
        communicationHandler = new CommunicationHandler(CloudPluginBootstrap.getInstance().getCloudServiceInfoHandler().getServiceName());

        getProxy().getPluginManager().registerListener(this, new CloudConnectListener());
        System.out.println(CloudPluginBootstrap.getInstance().getCloudServiceInfoHandler().getGroupVersion());
        System.out.println(CloudPluginBootstrap.getInstance().getCloudServiceInfoHandler().getGroupType());
        System.out.println(CloudPluginBootstrap.getInstance().getCloudServiceInfoHandler().getServiceName());

    }

    public CommunicationHandler getCommunicationHandler() {
        return communicationHandler;
    }

    public static BungeeBootstrap getBootstrap() {
        return bootstrap;
    }
}
