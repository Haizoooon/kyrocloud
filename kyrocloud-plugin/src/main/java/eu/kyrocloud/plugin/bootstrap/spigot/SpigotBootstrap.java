package eu.kyrocloud.plugin.bootstrap.spigot;

/*

  » eu.kyrocloud.plugin.bootstrap.spigot

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 21.03.2021 / 13:34

 */

import eu.kyrocloud.plugin.CloudPluginBootstrap;
import eu.kyrocloud.plugin.bootstrap.spigot.commands.ListCommand;
import eu.kyrocloud.plugin.netty.CommunicationHandler;
import org.bukkit.plugin.java.JavaPlugin;

public class SpigotBootstrap extends JavaPlugin {

    private static SpigotBootstrap bootstrap;
    private CommunicationHandler communicationHandler;

    public void onEnable() {
        bootstrap = this;
        new CloudPluginBootstrap();

        getCommand("cloudlist").setExecutor(new ListCommand());

    }

    public void register(){
        communicationHandler = new CommunicationHandler(CloudPluginBootstrap.getInstance().getCloudServiceInfoHandler().getServiceName());
    }

    public static SpigotBootstrap getBootstrap() {
        return bootstrap;
    }
}
