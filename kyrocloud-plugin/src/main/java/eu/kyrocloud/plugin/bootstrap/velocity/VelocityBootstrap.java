package eu.kyrocloud.plugin.bootstrap.velocity;

/*

  » eu.kyrocloud.plugin.bootstrap.velocity

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 21.03.2021 / 13:34

 */

import com.google.inject.Inject;
import com.velocitypowered.api.plugin.Plugin;
import com.velocitypowered.api.proxy.ProxyServer;
import eu.kyrocloud.plugin.CloudPluginBootstrap;
import eu.kyrocloud.plugin.netty.CommunicationHandler;
import org.slf4j.Logger;

@Plugin(id = "kyrocloud-plugin", name = "kyrocloud-plugin", authors = {"Haizoooon", "YyTFlo", "DeVii"})
public class VelocityBootstrap {

    private final Logger logger;
    private final ProxyServer proxyServer;
    private CommunicationHandler communicationHandler;

    @Inject
    public VelocityBootstrap(Logger logger, ProxyServer proxyServer) {
        this.logger = logger;
        this.proxyServer = proxyServer;

        new CloudPluginBootstrap();

    }

    public void register(){
        communicationHandler = new CommunicationHandler(CloudPluginBootstrap.getInstance().getCloudServiceInfoHandler().getServiceName());
    }

}
