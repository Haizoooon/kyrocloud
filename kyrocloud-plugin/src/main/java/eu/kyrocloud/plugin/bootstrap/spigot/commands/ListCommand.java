package eu.kyrocloud.plugin.bootstrap.spigot.commands;

/*

  » eu.kyrocloud.plugin.bootstrap.spigot.commands

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 23.03.2021 / 14:52

 */

import eu.kyrocloud.api.CloudAPI;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class ListCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(args.length == 1){
            if(args[0].equalsIgnoreCase("list")){
                CloudAPI.getInstance().getCachedCloudServices().forEach(cloudService -> {
                    System.out.println(cloudService.getServiceIdName() + " #" + cloudService.getPort());
                });
            }
        }

        System.out.println("TEST #1");

        return false;
    }
}
