package eu.kyrocloud.api.service;

/*

  » eu.kyrocloud.api.service

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 21.03.2021 / 10:28

 */

public enum ServiceState {

    PREPARING("PREPARING"),
    STARTING("STARTING"),
    STARTED("STARTED"),
    CLOSED("CLOSED");

    String display;

    ServiceState(String display) {
        this.display = display;
    }

    public String getDisplay() {
        return display;
    }
}
