package eu.kyrocloud.api.service;

/*

  » eu.kyrocloud.api.service

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 22.03.2021 / 21:23

 */

import eu.kyrocloud.api.group.GroupType;
import eu.kyrocloud.api.group.GroupVersion;

public interface ICloudService {

    void handleStart();
    void stop(boolean value);

    String getName();
    String getServiceIdName();
    int getPort();
    int getServiceId();
    GroupType getGroupType();
    GroupVersion getGroupVersion();
    ServiceState getServiceState();

}
