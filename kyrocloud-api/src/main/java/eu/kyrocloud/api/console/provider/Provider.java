package eu.kyrocloud.api.console.provider;

/*

  » eu.kyrocloud.api.console.provider

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 20.03.2021 / 11:48

 */

import java.util.ArrayList;
import java.util.List;

public class Provider {

    private final List<Object> suggestions;

    public Provider() {
        this.suggestions = new ArrayList<>();
    }

    public List<Object> getSuggestions() {
        return suggestions;
    }
}
