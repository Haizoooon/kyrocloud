package eu.kyrocloud.api.console;

/*

  » eu.kyrocloud.launcher.console.logger

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 13.03.2021 / 22:58

 */

public enum LogType {

    DEBUG("Debug", Colors.BLUE),
    INFO("Info", Colors.MAGENTA),
    ERROR("Error", Colors.RED),
    WARNING("Warn", Colors.YELLOW),
    SETUP("Setup", Colors.CYAN);

    private String display;
    private Colors colors;

    LogType(String display, Colors colors) {
        this.display = display;
        this.colors = colors;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public Colors getColors() {
        return colors;
    }

    public void setColors(Colors colors) {
        this.colors = colors;
    }
}
