package eu.kyrocloud.api.template;

/*

  » eu.kyrocloud.api.template

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 22.03.2021 / 22:11

 */

import java.util.UUID;

public abstract class ITemplate {

    private String name;

    public ITemplate(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
