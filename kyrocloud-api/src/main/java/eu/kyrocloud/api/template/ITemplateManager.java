package eu.kyrocloud.api.template;

/*

  » eu.kyrocloud.api.template

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 23.03.2021 / 09:45

 */

public interface ITemplateManager {

    void deleteTemplate(String name);
    void createTemplate(String name);

}
