package eu.kyrocloud.api.exception;

/*

  » eu.kyrocloud.api.exception

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 21.03.2021 / 11:38

 */

public class GroupFailureRegisterException extends Exception {

    public GroupFailureRegisterException() {
        super();
    }

    public GroupFailureRegisterException(String message) {
        super(message);
    }

    public GroupFailureRegisterException(String message, Throwable cause) {
        super(message, cause);
    }

    public GroupFailureRegisterException(Throwable cause) {
        super(cause);
    }

    public GroupFailureRegisterException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
