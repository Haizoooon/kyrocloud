package eu.kyrocloud.api.exception;

/*

  » eu.kyrocloud.api.exception

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 21.03.2021 / 11:38

 */

public class GroupFailureUnregisterException extends Exception {

    public GroupFailureUnregisterException() {
        super();
    }

    public GroupFailureUnregisterException(String message) {
        super(message);
    }

    public GroupFailureUnregisterException(String message, Throwable cause) {
        super(message, cause);
    }

    public GroupFailureUnregisterException(Throwable cause) {
        super(cause);
    }

    public GroupFailureUnregisterException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
