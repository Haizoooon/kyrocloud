package eu.kyrocloud.api.exception;

/*

  » eu.kyrocloud.api.exception

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 21.03.2021 / 11:05

 */

public class CommandFailureRegisterException extends Exception {

    public CommandFailureRegisterException() {
        super();
    }

    public CommandFailureRegisterException(String message) {
        super(message);
    }

    public CommandFailureRegisterException(String message, Throwable cause) {
        super(message, cause);
    }

    public CommandFailureRegisterException(Throwable cause) {
        super(cause);
    }

    public CommandFailureRegisterException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
