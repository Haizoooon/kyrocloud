package eu.kyrocloud.api.commands;

/*

  » eu.kyrocloud.api.commands

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 26.03.2021 / 13:42

 */

import eu.kyrocloud.api.console.LogType;

public interface ICommandSender {

    void sendMessage(String message);
    void sendMessage(LogType logType, String message);
    boolean hasPermission(String permission);

}
