package eu.kyrocloud.api.commands;

/*

  » eu.kyrocloud.api.commands

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 13.03.2021 / 22:59

 */

import com.google.common.collect.Lists;
import eu.kyrocloud.api.console.Colors;
import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.api.exception.CommandFailureRegisterException;
import org.jline.reader.Candidate;

import java.util.ArrayList;
import java.util.List;

public abstract class IConsoleCommand {

    public static List<IConsoleCommand> commands = new ArrayList<>();
    private String command;

    public IConsoleCommand(){
        register();
    }

    public void register(){
        if(getClass().getAnnotation(Command.class) == null){
            try {
                throw new CommandFailureRegisterException("Command " + getClass().getName() + " could not be registered");
            } catch (CommandFailureRegisterException e) {
                e.printStackTrace();
            }
            return;
        }
        Command command = getClass().getAnnotation(Command.class);
        this.command = command.name();
        commands.add(this);
    }

    public abstract List<Candidate> getSuggestions();

    public abstract void handle(ICommandSender sender, String[] args);

    public static List<IConsoleCommand> getCommands() {
        return commands;
    }

    public static void setCommands(List<IConsoleCommand> commands) {
        IConsoleCommand.commands = commands;
    }

    public abstract String[] getUsage();

    public String getCommand() {
        return command;
    }

}
