package eu.kyrocloud.api.commands;

/*

  » eu.kyrocloud.api.commands

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 21.03.2021 / 10:49

 */

public enum CommandType {

    INGAME,
    CONSOLE,
    CONSOLE_AND_INGAME;

}
