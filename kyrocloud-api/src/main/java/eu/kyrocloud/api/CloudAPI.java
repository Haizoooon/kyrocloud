package eu.kyrocloud.api;

/*

  » eu.kyrocloud.api

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 22.03.2021 / 22:08

 */

import com.google.common.collect.Lists;
import eu.kyrocloud.api.player.ICloudPlayer;
import eu.kyrocloud.api.service.ICloudService;
import eu.kyrocloud.api.template.ITemplate;

import java.util.List;

public abstract class CloudAPI {

    private final List<ICloudService> cachedCloudServices;
    private final List<ITemplate> cachedTemplates;
    private final List<ICloudPlayer> cachedCloudPlayers;

    private static CloudAPI instance;

    public CloudAPI() {
        instance = this;

        this.cachedCloudPlayers = Lists.newArrayList();
        this.cachedCloudServices = Lists.newArrayList();
        this.cachedTemplates = Lists.newArrayList();
    }

    public List<ICloudPlayer> getCachedCloudPlayers() {
        return cachedCloudPlayers;
    }

    public List<ITemplate> getCachedTemplates() {
        return cachedTemplates;
    }

    public List<ICloudService> getCachedCloudServices() {
        return cachedCloudServices;
    }

    public static CloudAPI getInstance() {
        return instance;
    }
}
