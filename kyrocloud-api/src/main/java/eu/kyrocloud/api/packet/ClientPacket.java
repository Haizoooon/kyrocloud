package eu.kyrocloud.api.packet;

/*

  » eu.kyrocloud.api.packet

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 21.03.2021 / 13:58

 */

public abstract class ClientPacket {

    private String packetName;

    public ClientPacket(String packetName) {
        this.packetName = packetName;
    }

    public abstract void read(String value);
    public abstract void write(String value);

    public String getPacketName() {
        return packetName;
    }

}
