package eu.kyrocloud.api.database;

import eu.kyrocloud.api.database.function.ISqlFunction;

import java.sql.ResultSet;

public interface ISqlExecutor {

    <T> T executeQuery(String query, ISqlFunction<ResultSet, T> function, T defaultValue);

    int executeUpdate(String query);

}
