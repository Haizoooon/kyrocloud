package eu.kyrocloud.api.database;

public enum  SqlDataType {

    TEXT("text"),
    VARCHAR("varchar", 64),
    INT("int");

    private final String sqlTag;
    private int length;

    SqlDataType(String sqlTag) {
        this.sqlTag = sqlTag;
    }

    SqlDataType(String sqlTag, int length){
        this.sqlTag = sqlTag;
        this.length = length;
    }

    public int getLength() {
        return length;
    }

    public SqlDataType setLength(int length) {
        this.length = length;
        return this;
    }

    public String getSqlTag() {
        if(this == SqlDataType.VARCHAR) return sqlTag + "(" + length + ")";
        else return sqlTag;
    }

}
