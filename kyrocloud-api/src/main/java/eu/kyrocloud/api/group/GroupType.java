package eu.kyrocloud.api.group;

/*

  » eu.kyrocloud.api.group

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 21.03.2021 / 16:07

 */

public enum GroupType {

    PROXY("Proxy"),
    SERVER("Server"),
    LOBBY("Lobby");

    String display;

    GroupType(String display) {
        this.display = display;
    }

    public String getDisplay() {
        return display;
    }
}
