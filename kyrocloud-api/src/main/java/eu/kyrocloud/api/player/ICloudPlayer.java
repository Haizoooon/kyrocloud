package eu.kyrocloud.api.player;

/*

  » eu.kyrocloud.api.player

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 21.03.2021 / 13:28

 */

import eu.kyrocloud.api.commands.ICommandSender;
import eu.kyrocloud.api.service.ICloudService;

public interface ICloudPlayer extends ICommandSender {

    /**
     * Sends the player a message
     * @param message
     */
    void sendMessage(String message);

    /**
     * Kicks the player with a specific reason
     * @param message
     */
    void kick(String message);

    /**
     * Kicks the player with default reason
     */
    void kick();

    void connect(ICloudService cloudService);

    /**
     * Sends the player a title
     * @param subTitle
     * @param title
     * @param fadeIn
     * @param stay
     * @param stayOut
     */

    void sendTitle(String subTitle, String title, int fadeIn, int stay, int stayOut);

}
