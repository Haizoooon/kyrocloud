package eu.kyrocloud.api.player;

/*

  » eu.kyrocloud.api.player

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 23.03.2021 / 11:36

 */

import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface ICloudPlayerManager {

    void registerPlayer(UUID uuid);
    Map<UUID, ICloudPlayer> getCachedCloudPlayers();
    ICloudPlayer getCloudPlayerByUniqueId(UUID uuid);

}
