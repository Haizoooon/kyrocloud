package eu.kyrocloud.api.netty;

/*

  » de.stickcloud.api.netty

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 10.03.2021 / 07:27

 */

public enum NettyProcess {

    SERVER_REGISTERED,
    SERVER_COMMAND,
    SERVER_PROXY_ADD,
    SERVER_PROXY_REMOVE,
    SERVER_PROXY_TABLIST,
    SERVER_PROXY_PLAYER_REGISTERED,
    SERVER_PROXY_PLAYER_CONNECTED,
    SERVER_PROXY_PLAYER_DISCONNECTED;

}
