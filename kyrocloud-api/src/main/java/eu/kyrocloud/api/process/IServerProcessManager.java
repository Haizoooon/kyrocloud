package eu.kyrocloud.api.process;

/*

  » eu.kyrocloud.api.process

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 22.03.2021 / 21:25

 */

import eu.kyrocloud.api.service.ICloudService;

import java.util.List;

public abstract class IServerProcessManager {

    private Process process;
    private Thread thread;

    private final ICloudService cloudService;

    public IServerProcessManager(ICloudService cloudService) {
        this.cloudService = cloudService;
    }

    public abstract void start();
    public abstract void stop();
    public abstract void deleteTempFiles();
    public abstract List<String> getStartArguments();
    public abstract void executeCommand(String command);

    public abstract ProcessBuilder createProcessBuilder();
    public abstract boolean isActive();

}
