package eu.kyrocloud.api.wrapper;

/*

  » eu.kyrocloud.base.wrapper.manager

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 24.03.2021 / 11:35

 */

public abstract class IWrapper {

    public String address;
    public String name;

    public IWrapper(String address, String name) {
        this.address = address;
        this.name = name;
    }

    public abstract void connect();

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
