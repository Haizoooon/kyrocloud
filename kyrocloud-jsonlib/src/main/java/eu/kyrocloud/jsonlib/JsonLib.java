package eu.kyrocloud.jsonlib;

/*

  » eu.kyrocloud.jsonlib

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 23.03.2021 / 11:46

 */

import org.json.JSONArray;
import org.json.JSONObject;

public class JsonLib {

    private final JSONObject jsonObject;

    public JsonLib(){
        jsonObject = new JSONObject();
    }

    public JsonLib append(String property, Object value){
        jsonObject.put(property, value);
        return this;
    }

    public JsonLib appendObject(String object, String property, Object value){
        JSONObject jsonElement = jsonObject.getJSONObject(object);
        jsonElement.put(property, value);
        return this;
    }

    public JsonLib appendArray(String object, Object value){
        JSONArray jsonElement = jsonObject.getJSONArray(object);
        jsonElement.put(value);
        return this;
    }

    public String appendAll(){
        return jsonObject.toString();
    }

}
