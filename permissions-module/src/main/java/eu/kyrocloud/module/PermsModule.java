package eu.kyrocloud.module;

import eu.kyrocloud.api.module.Module;
import eu.kyrocloud.launcher.console.logging.LoggerProvider;

@Module(name = "perms-module", version = "1.0.0-SNAPSHOT", authors = {"YyTFlo"})
public class PermsModule {

    private LoggerProvider logger;

    public void onInitialization(LoggerProvider logger){
        this.logger = logger;

        

    }

    public LoggerProvider getLogger() {
        return logger;
    }
}
