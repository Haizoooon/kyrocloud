package eu.kyrocloud.module.commands;

/*

  » eu.kyrocloud.module.commands

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 20.03.2021 / 11:18

 */

import eu.kyrocloud.api.commands.Command;
import eu.kyrocloud.api.commands.CommandType;
import eu.kyrocloud.api.commands.ICommandSender;
import eu.kyrocloud.api.commands.IConsoleCommand;
import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.launcher.console.ConsoleSender;
import eu.kyrocloud.module.ProxyModule;
import org.jline.reader.Candidate;

import java.util.Arrays;
import java.util.List;

@Command(name = "proxy", type = CommandType.CONSOLE_AND_INGAME)
public class ProxyCommand extends IConsoleCommand {

    @Override
    public void handle(ICommandSender sender, String[] args) {
        if (args[0].equalsIgnoreCase(getCommand())) {
            if(args.length == 1){
                Arrays.stream(getUsage()).forEach(usage -> ProxyModule.getInstance().getLogger().write(LogType.INFO, usage));
                return;
            }
            if(args.length >= 2){
                switch (args[1]){

                    case "whitelist":
                        sender.sendMessage(LogType.INFO, "whitelist add <Player>");
                        sender.sendMessage(LogType.INFO, "whitelist remove <Player>");
                        sender.sendMessage(LogType.INFO, "whitelist list");
                        break;



                }
            }
        }
    }

    @Override
    public List<Candidate> getSuggestions() {
        return null;
    }

    @Override
    public String[] getUsage() {
        return new String[0];
    }
}
