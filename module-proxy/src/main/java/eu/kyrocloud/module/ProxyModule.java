package eu.kyrocloud.module;

/*

  » eu.kyrocloud.module

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 16.03.2021 / 20:55

 */

import eu.kyrocloud.api.group.GroupType;
import eu.kyrocloud.api.module.Module;
import eu.kyrocloud.base.CloudBase;
import eu.kyrocloud.base.service.CloudService;
import eu.kyrocloud.jsonlib.JsonLib;
import eu.kyrocloud.launcher.console.logging.LoggerProvider;
import eu.kyrocloud.module.bootstrap.ProxyHandler;
import eu.kyrocloud.module.bootstrap.tablist.TablistConfiguration;

@Module(name = "proxy-module", version = "1.0-SNAPSHOT", authors = {"Haizoooon", "DeVii", "YyTFlo"})
public class ProxyModule {

    private static ProxyModule instance;
    private LoggerProvider logger;

    public void onInitialization(LoggerProvider logger){
        instance = this;

        this.logger = logger;

    }



    public static ProxyModule getInstance() {
        return instance;
    }

    public LoggerProvider getLogger() {
        return logger;
    }

}
