package eu.kyrocloud.module.events.velocity;

/*

  » eu.kyrocloud.module.events.velocity

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 19.03.2021 / 17:48
*/

import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.connection.LoginEvent;
import com.velocitypowered.api.proxy.Player;
import net.kyori.adventure.text.Component;

public class VelocityConnectEvent {

    @Subscribe
    public void handle(LoginEvent event){

        Player player = event.getPlayer();
        player.getTabList().setHeaderAndFooter(Component.text("\n test 1 \n"), Component.text("\n test 2 \n"));

    }

}
