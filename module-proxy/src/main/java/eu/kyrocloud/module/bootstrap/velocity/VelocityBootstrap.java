package eu.kyrocloud.module.bootstrap.velocity;

/*

  » eu.kyrocloud.module.bootstrap

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 19.03.2021 / 17:49

 */

import com.google.inject.Inject;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.proxy.ProxyInitializeEvent;
import com.velocitypowered.api.plugin.Plugin;
import com.velocitypowered.api.proxy.ProxyServer;
import eu.kyrocloud.module.events.velocity.VelocityConnectEvent;
import org.slf4j.Logger;

@Plugin(id = "kyrocloud-proxy-module", name = "kyrocloud-proxy-module")
public class VelocityBootstrap {

    private final ProxyServer proxyServer;
    private final Logger logger;

    @Inject
    public VelocityBootstrap(ProxyServer proxyServer, Logger logger) {
        this.proxyServer = proxyServer;
        this.logger = logger;
    }

    @Subscribe
    public void onProxyInitialization(ProxyInitializeEvent event) {
        proxyServer.getEventManager().register(this, new VelocityConnectEvent());
    }

}
