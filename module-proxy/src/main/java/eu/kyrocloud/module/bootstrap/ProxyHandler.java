package eu.kyrocloud.module.bootstrap;

/*

  » eu.kyrocloud.module.bootstrap

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 20.03.2021 / 11:24

 */

import eu.kyrocloud.module.bootstrap.tablist.TablistConfiguration;

public class ProxyHandler {

    public String replaceString(String input){
        return input.replace("%PLAYERS%", "1").replace("%MAX_PLAYERS%", "50").replace("%PROXY%", "Proxy-1");
    }

    public TablistConfiguration getTablistConfiguration(){
        return new TablistConfiguration("\n Test 1 \n", "\n Test 2 \n");
    }

}
