package eu.kyrocloud.module.bootstrap.tablist;

/*

  » eu.kyrocloud.module.bootstrap.tablist

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 20.03.2021 / 11:26

 */

public class TablistConfiguration {

    private String footer, header;

    public TablistConfiguration(String footer, String header) {
        this.footer = footer;
        this.header = header;
    }

    public String getFooter() {
        return footer;
    }

    public String getHeader() {
        return header;
    }
}
