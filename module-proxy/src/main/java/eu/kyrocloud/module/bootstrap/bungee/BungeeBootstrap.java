package eu.kyrocloud.module.bootstrap.bungee;

/*

  » eu.kyrocloud.module.bootstrap.bungee

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 20.03.2021 / 11:22

 */

import eu.kyrocloud.module.bootstrap.ProxyHandler;
import eu.kyrocloud.module.bootstrap.tablist.TablistConfiguration;
import eu.kyrocloud.module.events.bungee.BungeeConnectEvent;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.concurrent.TimeUnit;

public class BungeeBootstrap extends Plugin {

    private ProxyHandler proxyHandler;

    public void onEnable() {
        proxyHandler = new ProxyHandler();

        getProxy().getPluginManager().registerListener(this, new BungeeConnectEvent());



        startScheduler();
    }

    private void startScheduler(){
        getProxy().getScheduler().schedule(this, () -> {

            TablistConfiguration tablistConfiguration = proxyHandler.getTablistConfiguration();
            ProxyServer.getInstance().getPlayers().forEach(proxiedPlayer -> {
                sendHeaderAndFooter(proxiedPlayer, tablistConfiguration);
            });

        }, 1, 1, TimeUnit.SECONDS);
    }

    private void sendHeaderAndFooter(ProxiedPlayer proxiedPlayer, TablistConfiguration tablistConfiguration){
        String footer = tablistConfiguration.getFooter();
        String header = tablistConfiguration.getHeader();
        proxiedPlayer.setTabHeader(new TextComponent(proxyHandler.replaceString(header)), new TextComponent(proxyHandler.replaceString(footer)));
    }

}
