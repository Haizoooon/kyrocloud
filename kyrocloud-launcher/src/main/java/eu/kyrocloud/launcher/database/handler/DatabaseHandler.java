package eu.kyrocloud.launcher.database.handler;

import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.launcher.database.SqlAdapter;
import eu.kyrocloud.launcher.database.SqlConnector;
import eu.kyrocloud.launcher.database.SqlExecutor;
import eu.kyrocloud.launcher.database.security.AccessLoader;
import eu.kyrocloud.launcher.CloudBootstrap;
import eu.kyrocloud.launcher.setup.setups.ExampleSetup;

public class DatabaseHandler {

    private final SqlExecutor sqlBaseExecutor;
    private final AccessLoader accessLoader;
    private final SqlConnector sqlConnector;
    private final SqlAdapter baseSqlAdapter;

    public DatabaseHandler() {

        this.accessLoader = new AccessLoader();
        this.sqlConnector = new SqlConnector();

        CloudBootstrap.getInstance().getLogger().write(LogType.INFO, "Trying to connect to database...");

        if (accessLoader.isAccessJsonExist()) {
            this.sqlConnector.connect(this.accessLoader.loadObject());
        } else {
            //TODO: start database setup
            //new ExampleSetup();
        }

        this.sqlBaseExecutor = new SqlExecutor(this.getSqlConnector().getConnection());
        this.baseSqlAdapter = new SqlAdapter(this.sqlBaseExecutor);
    }

    public void disconnect() {
        if (accessLoader.isAccessJsonExist()) {
            this.sqlConnector.disconnect();
        }
    }

    public AccessLoader getAccessLoader() {
        return accessLoader;
    }

    public SqlConnector getSqlConnector() {
        return sqlConnector;
    }

    public SqlExecutor getSqlBaseExecutor() {
        return sqlBaseExecutor;
    }

    public SqlAdapter getBaseSqlAdapter() {
        return baseSqlAdapter;
    }

}
