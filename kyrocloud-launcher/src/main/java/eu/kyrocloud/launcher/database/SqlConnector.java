package eu.kyrocloud.launcher.database;

import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.launcher.database.security.SqlSecurityJsonObject;
import eu.kyrocloud.launcher.CloudBootstrap;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SqlConnector {

    private Connection connection;

    public void connect(SqlSecurityJsonObject sqlSecurityJsonObject) {
        try{
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            connection = DriverManager.getConnection("jdbc:mysql://" + sqlSecurityJsonObject.getHostname() + ":3306/" +
                            sqlSecurityJsonObject.getDatabase() + "?useJDBCCompliantTimezoneShift=true&&serverTimezone=UTC&&useUnicode=true&autoReconnect=true",
                    sqlSecurityJsonObject.getUsername(), sqlSecurityJsonObject.getPassword());
        }catch (Exception ex){
            ex.printStackTrace();
        }
        CloudBootstrap.getInstance().getLogger().write(LogType.INFO, "Successfully connected to database!");
    }

    public void disconnect() {
        try {
            this.connection.close();
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
    }

    public Connection getConnection() {
        return connection;
    }

}
