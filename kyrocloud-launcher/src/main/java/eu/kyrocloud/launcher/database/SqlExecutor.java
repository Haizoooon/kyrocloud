package eu.kyrocloud.launcher.database;

import eu.kyrocloud.api.database.ISqlExecutor;
import eu.kyrocloud.api.database.function.ISqlFunction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SqlExecutor implements ISqlExecutor {

    private final Connection connection;

    public SqlExecutor(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T> T executeQuery(String query, ISqlFunction<ResultSet, T> function, T defaultValue) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                return function.apply(resultSet);
            } catch (Exception throwable) {
                return defaultValue;
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return defaultValue;
    }

    @Override
    public int executeUpdate(String query) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            return preparedStatement.executeUpdate();
        } catch (SQLException exception) {
            exception.printStackTrace();
            return -1;
        }
    }

}
