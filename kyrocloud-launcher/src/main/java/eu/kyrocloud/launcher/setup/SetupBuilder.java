package eu.kyrocloud.launcher.setup;

/*

  » eu.kyrocloud.launcher.setup

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 25.03.2021 / 12:10

 */

import com.google.common.collect.Lists;
import eu.kyrocloud.api.commands.IConsoleCommand;
import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.launcher.CloudBootstrap;
import eu.kyrocloud.launcher.setup.interfaces.ISetup;
import eu.kyrocloud.launcher.setup.types.SetupEnd;
import eu.kyrocloud.launcher.setup.types.SetupInput;

import java.util.ArrayList;
import java.util.List;

public class SetupBuilder {

    private ArrayList<ISetup> setupQueue;

    private SetupInput currentInput;
    private SetupEnd setupEnd;
    private SetupInput[] setupInputs;
    private int currentIndex = 0;
    private ISetup currentSetup;

    private String prefix = CloudBootstrap.getInstance().getConsoleManager().prefix;

    public SetupBuilder(){
        setupQueue = Lists.newArrayList();
    }

    public void register(ISetup setup, SetupEnd setupEnd, SetupInput... setupInputs){
        CloudBootstrap.getInstance().setSetup(true);
        this.setupQueue.add(setup);
        this.currentSetup = setup;
        this.setupEnd = setupEnd;
        this.setupInputs = setupInputs;
        this.currentInput = setupInputs[currentIndex];
        CloudBootstrap.getInstance().getConsoleManager().getConsoleCompleter().setSuggestions(currentInput.getSuggestions());
        CloudBootstrap.getInstance().getLogger().write(LogType.SETUP, currentInput.getQuestion());
        nextQuestion(currentInput.handle(CloudBootstrap.getInstance().getConsoleManager().lineReader.readLine(prefix)));
    }

    public void nextQuestion(boolean value){
        if(value){
            if(this.currentIndex == this.setupInputs.length - 1){
                this.setupEnd.handleEnd();
                this.setupQueue.remove(this.currentSetup);
                CloudBootstrap.getInstance().setSetup(false);
                List<String> suggestions = Lists.newArrayList();

                for(IConsoleCommand command : IConsoleCommand.getCommands()){
                    String commandName = command.getCommand();
                    suggestions.add(commandName);
                }

                CloudBootstrap.getInstance().getConsoleManager().getConsoleCompleter().setSuggestions(suggestions);
                return;
            }
            this.currentIndex = this.currentIndex + 1;
            this.currentInput = this.setupInputs[this.currentIndex];
        }
        CloudBootstrap.getInstance().getConsoleManager().getConsoleCompleter().setSuggestions(currentInput.getSuggestions());
        CloudBootstrap.getInstance().getLogger().write(LogType.SETUP, currentInput.getQuestion());
        nextQuestion(currentInput.handle(CloudBootstrap.getInstance().getConsoleManager().lineReader.readLine(prefix)));
    }

    public ArrayList<ISetup> getSetupQueue() {
        return setupQueue;
    }

    public void setSetupQueue(ArrayList<ISetup> setupQueue) {
        this.setupQueue = setupQueue;
    }

    public SetupInput getCurrentInput() {
        return currentInput;
    }

    public void setCurrentInput(SetupInput currentInput) {
        this.currentInput = currentInput;
    }

    public SetupEnd getSetupEnd() {
        return setupEnd;
    }

    public void setSetupEnd(SetupEnd setupEnd) {
        this.setupEnd = setupEnd;
    }

    public SetupInput[] getSetupInputs() {
        return setupInputs;
    }

    public void setSetupInputs(SetupInput[] setupInputs) {
        this.setupInputs = setupInputs;
    }

    public int getCurrentIndex() {
        return currentIndex;
    }

    public void setCurrentIndex(int currentIndex) {
        this.currentIndex = currentIndex;
    }

}
