package eu.kyrocloud.launcher.setup.types;

/*

  » eu.kyrocloud.launcher.setup.types

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 23.03.2021 / 18:21

 */

import java.util.List;

public abstract class SetupInput {

    private String question;

    public SetupInput(String question){
        this.question = question;
    }

    public abstract List<String> getSuggestions();

    public abstract boolean handle(String input);

    public String getQuestion() {
        return question;
    }
}
