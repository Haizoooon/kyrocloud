package eu.kyrocloud.launcher.setup.setups;

/*

  » eu.kyrocloud.launcher.setup.setups

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 23.03.2021 / 18:58

 */

import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.launcher.CloudBootstrap;
import eu.kyrocloud.launcher.setup.interfaces.ISetup;
import eu.kyrocloud.launcher.setup.types.SetupEnd;
import eu.kyrocloud.launcher.setup.types.SetupInput;

import java.util.Arrays;
import java.util.List;

public class ExampleSetup implements ISetup {

    private String language;

    public ExampleSetup(){
        CloudBootstrap.getInstance().getSetupBuilder().register(this, new SetupEnd() {
            @Override
            public void handleEnd() {

                CloudBootstrap.getInstance().getLogger().write(LogType.DEBUG, language);

            }
        }, new SetupInput("Please select the language") {
            @Override
            public List<String> getSuggestions() {
                return Arrays.asList("en", "de");
            }

            @Override
            public boolean handle(String input) {
                language = input.replace(" ", "");
                return getSuggestions().contains(language);
            }
        });
    }

}
