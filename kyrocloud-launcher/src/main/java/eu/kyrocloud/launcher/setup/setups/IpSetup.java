package eu.kyrocloud.launcher.setup.setups;

/*

  » eu.kyrocloud.launcher.setup.setups

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 23.03.2021 / 22:08

 */

import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.launcher.CloudBootstrap;
import eu.kyrocloud.launcher.setup.SetupBuilder;
import eu.kyrocloud.launcher.setup.interfaces.ISetup;
import eu.kyrocloud.launcher.setup.types.SetupEnd;
import eu.kyrocloud.launcher.setup.types.SetupInput;
import eu.kyrocloud.launcher.url.UrlReader;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public class IpSetup implements ISetup {

    private String answer;
    private String address;

    private SetupBuilder setupBuilder = new SetupBuilder();

    public IpSetup(){
        setupBuilder.register(this, new SetupEnd() {
            @Override
            public void handleEnd() {
                if(answer.equalsIgnoreCase("yes")){
                    String content = new UrlReader().getContent("https://api.ipify.org/?format=json");
                    JSONObject jsonObject = new JSONObject(content);
                    address = jsonObject.getString("ip");
                    CloudBootstrap.getInstance().getLogger().write(LogType.DEBUG, address);
                    return;
                }
                startNonAutoIp();
            }
        }, new SetupInput("Do you want to receive the ip automatically? (ipify.org)") {
            @Override
            public List<String> getSuggestions() {
                return Arrays.asList("yes", "no");
            }

            @Override
            public boolean handle(String input) {
                answer = input;
                return getSuggestions().contains(input.replace(" ", ""));
            }
        });
    }

    public void startNonAutoIp(){
        setupBuilder.register(this, new SetupEnd() {
            @Override
            public void handleEnd() {
                CloudBootstrap.getInstance().getLogger().write(LogType.DEBUG, address);
            }
        }, new SetupInput("How should the address?") {
            @Override
            public List<String> getSuggestions() {
                return null;
            }

            @Override
            public boolean handle(String input) {
                address = input;
                return isAddress(input);
            }
        });
    }

    public boolean isAddress(String input){
        return input.split("\\.").length == 4;
    }

}
