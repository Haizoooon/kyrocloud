package eu.kyrocloud.launcher.setup.types;

/*

  » eu.kyrocloud.launcher.setup.types

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 23.03.2021 / 18:22

 */

public abstract class SetupEnd {

    public abstract void handleEnd();

}
