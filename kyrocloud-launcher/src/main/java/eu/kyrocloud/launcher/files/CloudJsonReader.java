package eu.kyrocloud.launcher.files;

/*

  » de.stickcloud.launcher.files

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 10.03.2021 / 10:35

 */

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CloudJsonReader {

    private JSONObject jsonObject;

    public CloudJsonReader(String content) {
        this.jsonObject = new JSONObject(content);
    }

    public Object read(String var1){
        return jsonObject.get(var1);
    }

}
