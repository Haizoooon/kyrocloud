package eu.kyrocloud.launcher.files;

/*

  » de.stickcloud.launcher.files

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 10.03.2021 / 08:27

 */

import eu.kyrocloud.launcher.CloudBootstrap;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class CloudJsonFileWriter {

    private File file;
    private FileWriter fileWriter;
    private JSONObject jsonObject;

    public CloudJsonFileWriter(String folder, String file) {
        this.file = new File(folder, file);
        this.jsonObject = new JSONObject();
        try {
            this.fileWriter = new FileWriter(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public CloudJsonFileWriter write(String var1, Object var2){
        jsonObject.put(var1, var2);
        return this;
    }

    public void execute(){
        try {
            fileWriter.write(jsonObject.toString());
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
