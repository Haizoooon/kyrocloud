package eu.kyrocloud.launcher.modules;

/*

  » eu.kyrocloud.launcher.modules

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 16.03.2021 / 20:10

 */

import eu.kyrocloud.api.console.Colors;
import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.launcher.CloudBootstrap;
import eu.kyrocloud.launcher.files.CloudJsonReader;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;

public class ModuleManager {

    private ModuleInitializer moduleInitializer;
    public List<Module> modules;

    public ModuleManager() {
        this.modules = new ArrayList<>();
        this.moduleInitializer = new ModuleInitializer();
    }

    private File getDirectory(){
        return new File("modules");
    }

    public void registerModules(){
        File[] files = getDirectory().listFiles();
        if(files == null){
            CloudBootstrap.getInstance().getLogger().write(LogType.WARNING + "Cannot find modules...");
            return;
        }

        JarFile jarFile = null;

        for(File file : Arrays.stream(files).filter(Objects::nonNull).filter(this::isJarFile).collect(Collectors.toList())){
            try {

                jarFile = new JarFile(file);
                JarEntry jarEntry = jarFile.getJarEntry("kyro-module.json");

                CloudJsonReader jsonReader;

                try (InputStream inputStream = jarFile.getInputStream(jarEntry)){
                    String content = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
                    jsonReader = new CloudJsonReader(content);
                }

                moduleInitializer.initModule(file.getName(), new KyroModuleDescription(jsonReader.read("name"), jsonReader.read("main")));


            } catch (IOException | NullPointerException e) {
                e.printStackTrace();
            } finally {
                try {
                    assert jarFile != null;
                    jarFile.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public void unregisterModules(){
        modules.forEach(module -> {
            CloudBootstrap.getInstance().getLogger().write(LogType.INFO, "Module " + Colors.GREEN + module.getName() + Colors.RESET + " was unregistered!");
            modules.remove(module);
        });
    }


    public File findModule(String name) { return new File("modules", name); }


    public static <T> T checkNotNull(T reference, Object errorMessage) {
        if (reference == null) {
            throw new NullPointerException(String.valueOf(errorMessage));
        }
        return reference;
    }

    public boolean isJarFile(File file){
        return file.getName().endsWith(".jar");
    }

    public List<Module> getModules() {
        return modules;
    }
}
