package eu.kyrocloud.launcher.modules;

/*

  » eu.kyrocloud.launcher.modules

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 16.03.2021 / 20:05

 */

import com.google.common.collect.Lists;

import java.util.List;

public class KyroModuleDescription {

    private String name, main;
    public static final List<KyroModuleDescription> descriptions = Lists.newArrayList();

    public KyroModuleDescription(Object name, Object main) {
        this.name = String.valueOf(name);
        this.main = String.valueOf(main);

        descriptions.add(this);
    }

    public String getName() {
        return name;
    }

    public String getMain() {
        return main;
    }

}
