package eu.kyrocloud.launcher.modules;

/*

  » eu.kyrocloud.launcher.modules

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 16.03.2021 / 20:04

 */

import eu.kyrocloud.api.console.Colors;
import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.api.module.Module;
import eu.kyrocloud.launcher.CloudBootstrap;
import eu.kyrocloud.launcher.console.logging.LoggerProvider;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.regex.Pattern;

public class ModuleInitializer {

    public static final Pattern ID_PATTERN = Pattern.compile("[a-z][a-z0-9-_]{0,63}");

    public void initModule(String file, KyroModuleDescription kyroModuleDescription) {
        try {
            URLClassLoader classLoader = new URLClassLoader(new URL[]{ new ModuleManager().findModule(file).toURI().toURL()}, getClass().getClassLoader());

            Class<?> clazz = Class.forName(kyroModuleDescription.getMain(), true, classLoader);

            if (clazz.isEnum() || clazz.isInterface()) {
                return;
            }

            Object instance = clazz.newInstance();

            if (clazz.getAnnotation(Module.class) == null) {
                CloudBootstrap.getInstance().getLogger().write(LogType.ERROR, "Cannot load " + instance.getClass().getName() + ". Class does not contains " + Colors.GREEN + "@Module annotation");
                return;
            }

            Module module = clazz.getAnnotation(Module.class);

            if(ID_PATTERN.matcher(module.name()).matches()){

                String name = module.name();
                String version = module.version();
                String[] authors = module.authors();

                CloudBootstrap.getInstance().getLogger().write(LogType.INFO, "Loaded module " + Colors.GREEN + name + " v" + version);

                CloudBootstrap.getInstance().getModuleManager().getModules().add(new eu.kyrocloud.launcher.modules.Module(name, version, authors));

            }

            Method method = clazz.getMethod("onInitialization", LoggerProvider.class);
            method.invoke(instance, CloudBootstrap.getInstance().getLogger());

        } catch (MalformedURLException | IllegalAccessException | InstantiationException | ClassNotFoundException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

}
