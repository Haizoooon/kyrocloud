package eu.kyrocloud.launcher;

/*

  » eu.kyrocloud.launcher

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 13.03.2021 / 20:00

 */

import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.launcher.console.ConsoleManager;
import eu.kyrocloud.launcher.console.ConsoleSender;
import eu.kyrocloud.launcher.console.commands.CommandManager;
import eu.kyrocloud.launcher.database.handler.DatabaseHandler;
import eu.kyrocloud.launcher.files.FileHandler;
import eu.kyrocloud.launcher.console.logging.LoggerProvider;
import eu.kyrocloud.launcher.modules.ModuleManager;
import eu.kyrocloud.launcher.setup.SetupBuilder;

public class CloudBootstrap {

    public static CloudBootstrap instance;

    private final ModuleManager moduleManager;
    private final CommandManager commandManager;
    private final ConsoleManager consoleManager;
    private final FileHandler fileHandler;
    private final DatabaseHandler databaseHandler;
    private final SetupBuilder setupBuilder;
    private boolean setup = false;
    private final ConsoleSender consoleSender;

    private final LoggerProvider logger;

    public static void main(String[] args) {
        new CloudBootstrap();
    }

    public CloudBootstrap(){
        instance = this;

        this.fileHandler = new FileHandler();
        this.consoleManager = new ConsoleManager();
        this.setupBuilder = new SetupBuilder();
        this.logger = new LoggerProvider();
        this.databaseHandler = new DatabaseHandler();
        this.moduleManager = new ModuleManager();
        this.moduleManager.registerModules();
        this.commandManager = new CommandManager();
        this.consoleSender = new ConsoleSender();

    }

    public ConsoleSender getConsoleSender() {
        return consoleSender;
    }

    public SetupBuilder getSetupBuilder() {
        return setupBuilder;
    }

    public boolean isSetup() {
        return setup;
    }

    public void setSetup(boolean value){
        this.setup = value;
    }

    public DatabaseHandler getDatabaseHandler() {
        return databaseHandler;
    }

    public FileHandler getFileHandler() {
        return fileHandler;
    }

    public ConsoleManager getConsoleManager() {
        return consoleManager;
    }

    public CommandManager getCommandManager() {
        return commandManager;
    }

    public ModuleManager getModuleManager() {
        return moduleManager;
    }

    public LoggerProvider getLogger() {
        return logger;
    }

    public static CloudBootstrap getInstance() {
        return instance;
    }
}
