package eu.kyrocloud.launcher.console;

/*

  » eu.kyrocloud.launcher.console

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 20.03.2021 / 20:27

 */

import com.google.common.collect.Lists;
import eu.kyrocloud.api.commands.IConsoleCommand;
import eu.kyrocloud.api.group.GroupVersion;
import eu.kyrocloud.launcher.CloudBootstrap;
import org.jline.reader.Candidate;
import org.jline.reader.Completer;
import org.jline.reader.LineReader;
import org.jline.reader.ParsedLine;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ConsoleCompleter implements Completer {

    private List<String> suggestions;

    @Override
    public void complete(LineReader lineReader, ParsedLine parsedLine, List<Candidate> list) {

        if(suggestions.isEmpty()){
            return;
        }

        list.addAll(getCurrentSuggestions());

    }

    public List<Candidate> getCurrentSuggestions(){
        List<Candidate> candidates = Lists.newArrayList();
        for(String candidate : suggestions){
            candidates.add(new Candidate(candidate));
        }
        return candidates;
    }

    public void setSuggestions(List<String> suggestions) {
        this.suggestions = suggestions;
    }
}
