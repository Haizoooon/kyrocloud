package eu.kyrocloud.launcher.console.commands;

/*

  » eu.kyrocloud.launcher.commands

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 20.03.2021 / 11:07

 */

import eu.kyrocloud.api.commands.IConsoleCommand;
import eu.kyrocloud.launcher.commands.*;

public class CommandManager {

    public CommandManager(){
        registerCommands();
    }

    public void registerCommands(){

        new ModulesCommand();
        new HelpCommand();

    }

    public IConsoleCommand getCommandByName(String command){
        return IConsoleCommand.commands.stream().filter(cloudCommand -> cloudCommand.getCommand().equalsIgnoreCase(command)).findAny().orElse(null);
    }

}
