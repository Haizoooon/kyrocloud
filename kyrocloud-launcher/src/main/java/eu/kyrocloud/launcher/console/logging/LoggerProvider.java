package eu.kyrocloud.launcher.console.logging;

/*

  » eu.kyrocloud.launcher.console.logger

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 13.03.2021 / 20:00

 */

import eu.kyrocloud.api.console.Colors;
import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.launcher.CloudBootstrap;
import org.jline.reader.LineReader;
import org.jline.utils.InfoCmp;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoggerProvider extends Logger {

    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
    private final ArrayList<String> cachedMessages = new ArrayList<>();
    private FileWriter fileWriter;

    public LoggerProvider(){
        super("KyroCloudLogger", null);
        setLevel(Level.ALL);
        setUseParentHandlers(false);

        CloudBootstrap.getInstance().getFileHandler().createFolder("logs");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        CloudBootstrap.getInstance().getFileHandler().createFile("logs", "kyrolog-" + dateFormat.format(new Date()) + ".log");

        try {
            File file = new File("logs/" + "log-" + dateFormat.format(new Date()) + ".txt");
            fileWriter = new FileWriter(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        write("\n ____  __.                     _________ .__                   .___\n" +
                "|    |/ _|___.__._______  ____ \\_   ___ \\|  |   ____  __ __  __| _/\n" +
                "|      < <   |  |\\_  __ \\/  _ \\/    \\  \\/|  |  /  _ \\|  |  \\/ __ | \n" +
                "|    |  \\ \\___  | |  | \\(  <_> )     \\___|  |_(  <_> )  |  / /_/ | \n" +
                "|____|__ \\/ ____| |__|   \\____/ \\______  /____/\\____/|____/\\____ | \n" +
                "        \\/\\/                           \\/                       \\/ \n\n"+
                "by Haizoooon, DeVii and YyTFlo\nv.1.0.1-SNAPSHOT\n\n");
    }

    public void write(String message){
        System.out.print(message);
    }

    public void write(LogType logType, String message){
        Date date = new Date();
        String format = Colors.RESET + "» [" + simpleDateFormat.format(date) + "] × " + logType.getColors() + logType.getDisplay() + Colors.RESET + " ▏ " + message;

        LineReader lineReader = CloudBootstrap.getInstance().getConsoleManager().lineReader;
        lineReader.getTerminal().puts(InfoCmp.Capability.carriage_return);
        lineReader.getTerminal().writer().println(format);
        lineReader.getTerminal().flush();

        cachedMessages.add(simpleDateFormat.format(date) + " " + logType.getDisplay() + " - " + message);
        handleMessage(simpleDateFormat.format(date) + " " + logType.getDisplay() + " - " + message);
    }

    public void handleMessage(String message){
        try {
            fileWriter.write(message + "\r\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stopWriter(){
        try {
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void clear(){
        for(int i = 0; i < 150; i++){
            write(" ");
        }
    }

}
