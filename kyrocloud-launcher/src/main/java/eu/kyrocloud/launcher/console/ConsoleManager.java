package eu.kyrocloud.launcher.console;

/*

  » eu.kyrocloud.launcher.console

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 20.03.2021 / 20:33

 */

import com.google.common.base.Charsets;
import eu.kyrocloud.api.commands.IConsoleCommand;
import eu.kyrocloud.api.console.Colors;
import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.launcher.CloudBootstrap;
import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;

import java.io.IOException;

public class ConsoleManager {

    public LineReader lineReader;
    public Thread thread;
    private ConsoleCompleter consoleCompleter = new ConsoleCompleter();

    public String prefix = Colors.RESET + " × " + Colors.CYAN + "KyroCloud" + Colors.RESET + " » ";

    public ConsoleManager(){
        lineReader = createLineReader();
        startThread();
    }

    private LineReader createLineReader(){


        Terminal terminal = null;
        try {
            terminal = TerminalBuilder.builder().system(true).streams(System.in, System.out).encoding(Charsets.UTF_8).dumb(true).build();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return LineReaderBuilder.builder()
                .completer(consoleCompleter)
                .terminal(terminal)
                .option(LineReader.Option.DISABLE_EVENT_EXPANSION, true)
                .option(LineReader.Option.AUTO_REMOVE_SLASH, false)
                .option(LineReader.Option.INSERT_TAB, false)
                .build();

    }

    public void startThread(){
        thread = new Thread(() -> {
            String line;
            while (!Thread.currentThread().isInterrupted()){
                line = lineReader.readLine(prefix);
                handleInput(line);
            }
        });
        thread.start();
    }

    private void handleInput(String input){
        if(input.isEmpty()){
            return;
        }
        if(!CloudBootstrap.instance.isSetup()){
            String[] args = input.split(" ");
            if(CloudBootstrap.getInstance().getCommandManager().getCommandByName(args[0]) == null){
                CloudBootstrap.getInstance().getLogger().write(LogType.ERROR, "This command could not be found. Please use " + Colors.GREEN + "'help'" + Colors.RESET + " for all commands!");
                return;
            }
            IConsoleCommand command = CloudBootstrap.getInstance().getCommandManager().getCommandByName(args[0]);
            command.handle(CloudBootstrap.getInstance().getConsoleSender(), args);
            return;
        }
    }

    public void stopThread(){
        lineReader.getTerminal().reader().shutdown();
        lineReader.getTerminal().pause();
        thread.interrupt();
    }

    public ConsoleCompleter getConsoleCompleter() {
        return consoleCompleter;
    }

}
