package eu.kyrocloud.launcher.console;

/*

  » eu.kyrocloud.launcher.console

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 21.03.2021 / 16:43

 */

import eu.kyrocloud.api.commands.ICommandSender;
import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.launcher.CloudBootstrap;

public class ConsoleSender implements ICommandSender {

    @Override
    public void sendMessage(String message) {
        CloudBootstrap.getInstance().getLogger().write(message);
    }

    @Override
    public void sendMessage(LogType logType, String message) {
        CloudBootstrap.getInstance().getLogger().write(logType, message);
    }

    @Override
    public boolean hasPermission(String permission) {
        return false;
    }
}