package eu.kyrocloud.launcher.commands;

/*

  » eu.kyrocloud.launcher.commands

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 21.03.2021 / 10:04

 */

import eu.kyrocloud.api.commands.Command;
import eu.kyrocloud.api.commands.CommandType;
import eu.kyrocloud.api.commands.ICommandSender;
import eu.kyrocloud.api.commands.IConsoleCommand;
import eu.kyrocloud.api.console.Colors;
import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.launcher.CloudBootstrap;
import eu.kyrocloud.launcher.modules.Module;
import org.jline.reader.Candidate;

import java.util.List;

@Command(name = "modules", type = CommandType.CONSOLE)
public class ModulesCommand extends IConsoleCommand {

    @Override
    public void handle(ICommandSender sender, String[] args) {
        if (args[0].equalsIgnoreCase(getCommand())) {

            if(args.length == 2){
                if(args[1].equalsIgnoreCase("list")){
                    StringBuilder authors = new StringBuilder();
                    CloudBootstrap.getInstance().getLogger().write(LogType.INFO, " ");
                    CloudBootstrap.getInstance().getLogger().write(LogType.INFO, "These are all loaded modules");
                    for (Module module : CloudBootstrap.getInstance().getModuleManager().getModules()) {
                        for(String author : module.getAuthors()){
                            authors.append(author).append(", ");
                        }
                        CloudBootstrap.getInstance().getLogger().write(LogType.INFO, Colors.GREEN + module.getName() + " " + module.getVersion() + Colors.RESET + " by " + Colors.GREEN + authors.toString());
                    }
                    CloudBootstrap.getInstance().getLogger().write(LogType.INFO, " ");
                } else {
                    Module module = CloudBootstrap.getInstance().getModuleManager().getModules().stream().filter(modules -> modules.getName().equalsIgnoreCase(args[1])).findAny().orElse(null);
                    if(module == null){
                        CloudBootstrap.getInstance().getLogger().write(LogType.ERROR, "Module could not be found!");
                        return;
                    }
                    StringBuilder authors = new StringBuilder();
                    for(String author : module.getAuthors()){
                        authors.append(author).append(", ");
                    }
                    CloudBootstrap.getInstance().getLogger().write(LogType.INFO, " ");
                    CloudBootstrap.getInstance().getLogger().write(LogType.INFO, "Name - " + Colors.GREEN + module.getName());
                    CloudBootstrap.getInstance().getLogger().write(LogType.INFO, "Version - " + Colors.GREEN + module.getVersion());
                    CloudBootstrap.getInstance().getLogger().write(LogType.INFO, "Authors - " + Colors.GREEN + authors.toString());
                    CloudBootstrap.getInstance().getLogger().write(LogType.INFO, " ");
                }
            }

        }
    }

    @Override
    public List<Candidate> getSuggestions() {
        return null;
    }

    @Override
    public String[] getUsage() {
        return new String[0];
    }
}
