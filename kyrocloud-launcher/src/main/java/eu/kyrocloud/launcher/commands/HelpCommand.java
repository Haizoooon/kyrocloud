package eu.kyrocloud.launcher.commands;

/*

  » de.stickcloud.launcher.commands

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 10.03.2021 / 10:49

 */

import eu.kyrocloud.api.commands.Command;
import eu.kyrocloud.api.commands.CommandType;
import eu.kyrocloud.api.commands.ICommandSender;
import eu.kyrocloud.api.commands.IConsoleCommand;
import eu.kyrocloud.api.console.LogType;
import eu.kyrocloud.launcher.CloudBootstrap;
import eu.kyrocloud.launcher.setup.setups.ExampleSetup;
import org.jline.reader.Candidate;

import java.util.Arrays;
import java.util.List;

@Command(name = "help", type = CommandType.CONSOLE)
public class HelpCommand extends IConsoleCommand {

    @Override
    public void handle(ICommandSender sender, String[] args) {
        if (args[0].equals(getCommand())) {
            CloudBootstrap.getInstance().getLogger().write(" ");
            commands.forEach(line -> Arrays.stream(line.getUsage()).forEach(usage -> CloudBootstrap.getInstance().getLogger().write(LogType.INFO, usage)));
            CloudBootstrap.getInstance().getLogger().write(" ");

            new ExampleSetup();

        }
    }

    @Override
    public List<Candidate> getSuggestions() {
        return null;
    }

    @Override
    public String[] getUsage() {
        return new String[]{"help × Shows this help overview"};
    }
}
